<?php
use Swagger\Annotations as SWG;

/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="www.squares.seeds.cm",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Squares Ecosystem API (ง ͡ʘ ͜ʖ ͡ʘ)ง",
 *         description="Project for a complete management system of orders. Made with labour  💪🏾 and love 💖.〜(￣▽￣〜)",
 *         @SWG\Contact(
 *             email="info@seeds.cm"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about Swagger",
 *         url="http://swagger.io"
 *     )
 * )
 * @SWG\Tag(
 *   name="order",
 *   description="All app orders 🍽️"
 * )
 * @SWG\Tag(
 *   name="institute",
 *   description="All on admin 🏦"
 * )
 * @SWG\Tag(
 *   name="service",
 *   description="All about services 💇"
 * )
 * @SWG\Tag(
 *   name="product",
 *   description="Manipulations on products 🍜"
 * )
 * @SWG\Tag(
 *   name="employee",
 *   description="All on app employees 🤵"
 * )
 *  @SWG\Tag(
 *   name="customer",
 *   description="Everything about the customers 😊"
 * )
 * @SWG\Tag(
 *   name="general",
 *   description="Genaral app management ⚡"
 * )
 *  @SWG\Tag(
 *   name="user",
 *   description="Everything about your "
 * )
 *  @SWG\Tag(
 *   name="sortie",
 *   description="Everything about the system sortie 💻"
 * )
 * @SWG\Tag(
 *   name="admin",
 *   description="All on stats 📈"
 * )
 * @SWG\Tag(
 *   name="favoris",
 *   description="All on favorites 📈"
 * )


 */