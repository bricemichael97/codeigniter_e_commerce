<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This class deals with all response messages interactions with the application
 *
 * @package         GLobal Packages
 * @category        Model
 * @author          @steveDeSeed
 * @link            https://gitlab.com/steveDeSeed
 */

class Mailer {

    /**
     * @var $tabelName String The database name
     */
    public $tableName = "";

    /**
     * @var $email String receiver email
     */
    public $mail = null;

    /**
     * @var $language String mail language
     */
    public $language;

    /**
     * @var $datas String The binding datas
     */
    public $datas = [];

    /**
     * @var $email Email instance of CI email class
     */
    private $email;

    /**
     * @var $from String sender Email
     */
    public $from = "hub@seeds.cm";

    /**
     * @var $senderName String
     */
    public $senderName = 'E-CV';

    /**
     * @var $config Array
     */


    public $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.seeds.cm',
        'smtp_port' => 2525,
        'smtp_user' => 'hub@seeds.cm',
        'smtp_pass' => 'SeedSpace2018',
        'mailtype'  => 'html',
        'charset'   => 'utf-8'
    );

    /**
     * @var $ci CI instance
     */
    protected $CI;

    /**
     * Have to think about how to translate those messages
     * @param $langage String The mail language
     */

    public function __construct($config = array())
    {

        $this->CI =& get_instance();

    }


    public function sendMail($to, $subject, $datas, $templateName)
    {

        $this->CI->load->library("email", $this->config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->from($this->from, $this->senderName);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $sendingContent = $this->normalize($templateName, (array)$datas);

        $sendingContent = str_replace("nom_user", $datas["nom"], $sendingContent);
        $sendingContent = str_replace("user_prenom", $datas["prenom"], $sendingContent);

        $this->CI->email->message(utf8_decode($sendingContent));


        /*        echo $this->CI->email->print_debugger();
                die();*/
        return $this->CI->email->send();
    }

    public function sendMail_paiement($to, $subject, $datas, $templateName)
    {
        $this->CI->load->library("email", $this->config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->from($this->from, $this->senderName);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $sendingContent = $this->normalize($templateName, (array)$datas);


        $this->CI->email->message(utf8_decode($sendingContent));


        /*        echo $this->CI->email->print_debugger();
                die();*/
        return $this->CI->email->send();
    }

    /**
     * @param $templateName String
     * @param $datas Array
     * @return String sendable content
     */

    public function normalize($templateName, $datas)
    {

        $htmlTemplate = $this->get_include(__DIR__ . "./../templates/" . $templateName . ".html");

        foreach ($datas as $key => $value) {
            $regex = '#{{' . $key . '}}#';
            $htmlTemplate = preg_replace($regex, $value, $htmlTemplate);
        }
        return $htmlTemplate;
    }

    public function get_include($file)
    {
        ob_start();
        include($file);
        return ob_get_clean();
    }

    public function send_mail_to_admin($to = "info@seeds.cm", $subject, $datas, $templateName)
    {

        $this->CI->load->library("email", $this->config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->from($this->from, $this->senderName);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $sendingContent = $this->normalize($templateName, (array)$datas);
        $this->CI->email->message(utf8_decode($sendingContent));


        /*        echo $this->CI->email->print_debugger();
                die();*/
        return $this->CI->email->send();
    }

//    envoi de mail à une plage de personnes à partir de leurs @
/**
 * $datas = [lien de l'event, message à envoyer à l'user]
 * message = "Bonjour, une formation a été crée ou a été mise à jour sur Ufunde."
 * lien = "https://www.ufunde.cm/formation/id_formation"
 * */
    public function send_mail_update($to = [], $subject="Nouvelle Formation sur Ufunde", $datas, $templateName="update")
    {
        if(empty($to))
        {
            return true;
        }

        $abonnesT = ["nouyepsteve@gmail.com", "nouyepsteve@gmail.com", "nouyepsteve@gmail.com"];
        $this->CI->load->library("email", $this->config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->from($this->from, $this->senderName);
        $this->CI->email->bcc($to);
        $this->CI->email->subject($subject);
        $sendingContent = $this->normalize($templateName, (array)$datas);
        $this->CI->email->message(utf8_decode($sendingContent));


        /*        echo $this->CI->email->print_debugger();
                die();*/
        return $this->CI->email->send();
    }

    public function send_mail_newsletter($datas, $subject, $abonnes,$templateName)
    {
        $this->CI->load->library("email", $this->config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->set_crlf("\r\n");
        $this->CI->email->from($this->from, $this->senderName);
//        $abonnesT = ["nouyepsteve@gmail.com", "nouyepsteve@gmail.com", "nouyepsteve@gmail.com"];
        $this->CI->email->to($abonnes);
        $this->CI->email->subject(utf8_decode($subject));
        $sendingContent = $this->normalize($templateName, $datas);
        $this->CI->email->message(utf8_decode($sendingContent));
        $retour = $this->CI->email->send();

        return $retour;

    }

}