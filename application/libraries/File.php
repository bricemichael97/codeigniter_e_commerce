<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * PHP File uploader for Codeigniter
 *
 * @package            CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @porting author    SteveDeSeed
 *
 * @version        1.0
 */

class File
{

    public function uploader_image($image, $repertoire_user)
    {
        $target_path = './uploads/' . $repertoire_user . '/';
        $link = 'uploads/' . $repertoire_user . '/';
// Pour créer une stucture imbriquée, le paramètre $recursive
        // doit être spécifié.

        if (!file_exists($target_path)) {
            if (!mkdir($target_path, 0777, true)) {
                die('Echec lors de la création des répertoires...');
            }

        }

        $target_path = $target_path . basename($image['name']);
        $link = $link . basename($image['name']);
        // echo($image['name']);
        // echo($link);
        // print_r($target_path);
        if (move_uploaded_file($image['tmp_name'], $target_path)) {
            return $link;
        } else {
//            echo $target_path;
            return false;
        }

    }

    public function uploader_fichier($image, $repertoire)
    {

        $target_path = './uploads/' . $repertoire . '/';
        $link_event = 'uploads/' . $repertoire . '/';
// Pour créer une stucture imbriquée, le paramètre $recursive
        // doit être spécifié.

        if (!file_exists($target_path)) {
            if (!mkdir($target_path, 0777, true)) {
                die('Echec lors de la création des répertoires...');
            }

        }

        $target_path = $target_path . basename($image['name']);

        if (move_uploaded_file($image['tmp_name'], $target_path)) {
            return $link_event . basename($image['name']);
        } else {
//            echo $target_path;
            return false;
        }

    }

    public function uploader_video($image, $repertoire)
    {
        $config['upload_path'] = './uploads/' . $repertoire . '/videos/';
        $config['allowed_types'] = 'mp4|mp3|avi|mov|wmv|MP4|mkv|gif';
        $config['max_size'] = '102400KB';
        $config['overwrite'] = true;
        $config['file_name'] = time();
        $this->load->library('upload', $config);
        $this->upload->initialize($config); // Make sure it has been initialized

        if (!$this->upload->do_upload($image)) {
            $info = array('error' => $this->upload->display_errors());
            // $this->response($this->upload->display_errors(), REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            return $info;
        } else {
            $info = "good";
            return $info;
        }

    }

}