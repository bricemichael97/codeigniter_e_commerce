<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This class deals with all admin interactions with the application
 *
 * @package         ArticlePackage
 * @category        Controller
 * @author          @Melo
 * @link            https://gitlab.com/philippe1997
 */
class Evenement extends REST_Controller
{

    public $repertoire_couverture_evenement = "evenement";
    public $repertoire_ressouce_article = "referenceproduit";

    public $reponse = "";

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->reponse = new stdClass();
        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }
    public function index_get($page = 'evenement_post')
    {
        $this->view($page);
    }
    public function view($page = "evenement_post")
    {

        $data = array();
        $flow = $_SESSION['idvendeur'];
        $data['evenements'] = $this->evenement_model->get_evenement_fromvendeur($flow);
        $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
        $data['categories'] = $this->categorie_model->fetch_data();

        /* $data['evenements'] = $this->evenement_model->fetch_data(); */
        // $data['evenements'] = $this->evenement_model->get_evenements(0,3);

        // var_dump( $_SESSION);
        // return;

        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
    }


    public function handle_image_post()
    {
        if ($_FILES) {
            if (isset($_FILES["upload"])) {
                /*** upload de l'image du produit**/
                $_FILES['upload']["name"] = "ressouce_article" . time() . "." . $ext = pathinfo($_FILES["ressouce_article"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["ressouce_article"], $this->repertoire_ressouce_article);
                $this->reponse->url = base_url() . $chemin1;
            } else {
                $this->reponse->uploaded = false;
                $this->reponse->url = 'Label your file upload please';
            }
        } else {
            $this->reponse->uploaded = false;
            $this->reponse->url = 'No file';
        }
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
    public function event_post($page = "evenement_post")
    {
        $data = $this->post();

        // var_dump($data);
        // return;
        if ($_FILES) {
            if (isset($_FILES["imageevenement"])) {
                /*** upload de l'image du produit**/
                $_FILES['imageevenement']["name"] = "imageevenement" . time() . "." . $ext = pathinfo($_FILES["imageevenement"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imageevenement"], $this->repertoire_couverture_evenement);
                $data["imageevenement"] = base_url() . $chemin1;
            }
        }
        $data['date_debut_evenement'] = date("Y-m-d H:i:s");
        $data['idvendeur'] = $_SESSION['idvendeur'];
        $result = $this->evenement_model->add_evenement($data);
        if ($result) {
            // $this->notifyUsers('200', $user);
            $flow = $_SESSION['idvendeur'];
            $data['evenements'] = $this->evenement_model->get_evenement_fromvendeur($flow);
            $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
            $data['categories'] = $this->categorie_model->fetch_data();
            $data['result'] = $result;
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function evenement_post($page = "evenement_post")
    {
        $data = $this->post();

        var_dump($data);
        if ($_FILES) {
            if (isset($_FILES["imagearticle"])) {
                /*** upload de l'image du produit**/
                $_FILES['imagearticle']["name"] = "imagearticle" . time() . "." . $ext = pathinfo($_FILES["imagearticle"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagearticle"], $this->repertoire_couverture_article);
                $data["imagearticle"] = base_url() . $chemin1;
            }
        }

        $result = $this->evenement_model->add_evenement($data);
        if ($result) {
            // $this->notifyUsers('200', $user);
            $flow = $_SESSION['idvendeur'];
            $data['fetchdata'] = $this->evenement_model->get_evenements($flow);
            $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
            $data['categories'] = $this->categorie_model->fetch_data();

            $data['result'] = $result;
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function update_evenement_post($page = "evenement_post")
    {
        if (!$this->post()) {
            $this->empty_formatter();
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $data = $this->post();

        if ($_FILES) {
            $user_db = (array) $this->evenement_model->get_evenement_by_id($data['idevenement']);

            if (isset($_FILES["imageevenement"])) {
                if ($user_db["imageevenement"] != "" && $user_db["imageevenement"] != null && $user_db["imageevenement"] != "null") {
                    $lien = str_replace(base_url(), "", $user_db["imageevenement"]);
                    if (file_exists($lien)) {
                        unlink($lien);
                    }
                }
                $_FILES['imageevenement']["name"] = "imageevenement" . time() . "." . $ext = pathinfo($_FILES["imageevenement"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imageevenement"], $this->repertoire_couverture_evenement);
                $data["imageevenement"] = base_url() . $chemin1;
            }
        }
        $data['date_debut_evenement'] = date("Y-m-d H:i:s");
        $data['idvendeur'] = $_SESSION['idvendeur'];
        $result = $this->evenement_model->update_evenement($data);

        if ($result) {
            // var_dump($result);
            $flow = $_SESSION['idvendeur'];
            $data['evenements'] = $this->evenement_model->get_evenement_fromvendeur($flow);
            $data['result'] = $result;
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
            // $this->reponse->body = $result;
            // $this->reponse->status = 'success';
            // $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function evente_get($idevenement)
    {
        // $id = $this->uri->segment(3); 
        $page = "evenement_post";
        if ($idevenement) {
            $this->load->model("evenement_model");
            $delet = $this->evenement_model->delete_evenement($idevenement);

            if ($delet) {
                $flow = $_SESSION['idvendeur'];
                $data['evenements'] = $this->evenement_model->get_evenement_fromvendeur($flow);
                $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
                $data['categories'] = $this->categorie_model->fetch_data();

                $this->load->view('templates/header');
                $this->load->view('pages/' . $page, $data);
                $this->load->view('templates/footer');
                /*$this->reponse->status = 'success';
                $this->response($this->reponse, REST_Controller::HTTP_OK);*/
            } else {
                $this->reponse->status = 'not found';
                $this->response($this->reponse, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->reponse->status = 'not found';
        }
    }

    public function article_get()
    {

        $result = $this->User_model->get_article_by_id($this->get('id_article'));
        $this->reponse->body = $result;

        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }

    public  function evenements_get()
    {
        $data[] = $this->evenement_model->get_evenements();

        $this->reponse->body = $data;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
    public  function evenemontory_get($idevenement)
    {
        $data = $this->evenement_model->get_evenement_by_id($idevenement);

        $this->reponse->body = $data;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }

    public function articles_user_get()
    {
    }
    public function empty_formatter()
    {
        $this->reponse->code = "104";
        $this->reponse->body = new stdClass();
        $this->reponse->message = "Aucune donnée trouvé";
        $this->reponse->status = "failed";
    }
}
