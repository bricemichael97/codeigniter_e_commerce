<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * The general class used
 *
 * @package         UserPackage
 * @category        Controller
 * @author          @Melo
 * @link            https://gitlab.com/philippe9
 */
class Pages extends REST_Controller
{
    public $repertoire_couverture_article = "article";
    public $repertoire_ressouce_article = "referenceproduit";

    public $reponse = "";

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->reponse = new stdClass();
        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }

    /*** cette méthode permet de visualiser la page d'accueil ***/
    public function index_get($page = 'home')
    {

        $this->view($page);
    }

    public function form_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules("first_name", "First Name", 'required|alpha');
        $this->form_validation->set_rules("last_name", "Last Name", 'required|alpha');

        if ($this->form_validation->run()) {
            $this->load->views->pages("home");
        }
    }
    /*** cette méthode permet de visualiser une page ***/
    public function view($page = "home")
    {
        $data = array();
        $data['resource_articles'] = $this->article_model->fetch_resource();
        $data['articles_slick'] = array_chunk($this->article_model->get_pages(), 6);
        // $data['articles'] = $this->article_model->fetch_resource();
        // var_dump($data);
        // return;
        $data['articles'] = $this->article_model->get_articles(0,12);
        /* $data['evenements'] = $this->evenement_model->fetch_data(); */
<<<<<<< HEAD
        $data['evenements'] = $this->evenement_model->get_events(0, 20);
=======
        $data['evenements'] = $this->evenement_model->get_events(0, 3);
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
        $data['categories'] = $this->categorie_model->fetch_data();
        // $data['categories_menu'] = 
        //  $data['evenements'] = $this->evenement_model->get_evenements();  
        // $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

        // echo $hostname;
        // return;
        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
    }
    public function all_users_get()
    {
        $data['resource_articles'] = $this->article_model->fetch_resource();
        $data['articles'] = $this->article_model->fetch_resource();

        if ($data) {
            $this->reponse->body = $data;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public  function article_get($idarticle = "2")
    {
        $data = array();
        
        $data['articles'] = [];
        $data['articles'] = $this->article_model->fetch_resource();
        $data['article'] = $this->article_model->get_article_by_id($idarticle);
        $data['art'] = $this->article_model->get_article_by_id($idarticle);
        // var_dump($data['article']);
        // var_dump($data['art']);
        // return;
        $data['idarticle'] = $idarticle;
        $data['commentaires'] = $this->article_model->get_product_comments($idarticle);
        $data['resource_articles'] = $this->article_model->get_resource_by_id($idarticle);
        $data['articles'] = $this->categorie_model->get_categorie_products($data['article']->id_categorie);
        $data['share_link'] = current_url();
        $data['vendeur'] = $this->vendeur_model->get_vendeur_by_id($data['article']->idvendeur);
        $this->load->view('templates/header');
        $this->load->view('pages/article', $data);
        $this->load->view('templates/footer');
    }

    public  function evenements_get($idevenement = "2")
    {
        $data = array();
        $data['evenements'] = [];
        $data['evenement'] = $this->evenement_model->get_evenement_by_id($idevenement);

        $this->load->view('templates/header');
<<<<<<< HEAD
        $this->load->view('pages/afficheevents', $data);
=======
        $this->load->view('pages/event', $data);
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
        $this->load->view('templates/footer');
    }

    public  function categorie_get($idcategorie = "2")
    {

        $data = array();
        $data['evenements'] = [];
        $data['evenement'] = $this->categorie_model->fetch_data($idcategorie);
        $data['categories'] = $this->categorie_model->get_categorie_bys_id($idcategorie); 
        // $data['article'] = $this->article_model->get_article_by_id($idarticle);
        // var_dump($data);
        // return;
         $this->load->view('templates/header');
        $this->load->view('pages/event', $data);
        $this->load->view('templates/footer');
    }
   
    public  function categorie_products_get($idcategorie = "2")
    {

        $data = array();
        // $data['idcategorie'] = $idcategorie;
        $data['articles'] = $this->categorie_model->get_categorie_products($idcategorie);
        
        $data['categorie'] = $this->categorie_model->get_categorie_by_id($idcategorie);
        // var_dump($data['articles']);
        // return;
        $this->load->view('templates/header');
        $this->load->view('pages/categorie_show', $data);
        $this->load->view('templates/footer');
    }

    public  function produit_()
    {
        $data = $this->post();

        if ($_FILES) {
            if (isset($_FILES["imagearticle"])) {
                /*** upload de l'image du produit**/
                $_FILES['imagearticle']["name"] = "imagearticle" . time() . "." . $ext = pathinfo($_FILES["imagearticle"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagearticle"], $this->repertoire_couverture_article);
                $data["imagearticle"] = base_url() . $chemin1;
            }
        }

        $article = $this->article_model->add_article($data);

        /*

        $this->load->view('templates/header');
        $this->load->view('pages/dashboard');
        $this->load->view('templates/footer');*/
    }
    public function chatPage_get($id)
    {
        // if(isset($_SESSION['user']->id_structure)){
        $data['title'] = "Discussion";
        $data['idvendeur'] = $id;
        $data['vendeur'] = $this->vendeur_model->get_vendeur_by_id($id);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/chat');
        $this->load->view('templates/footer');
        // }
    }
    public function mychatPage_get($id)
    {
        // if(isset($_SESSION['user']->id_structure)){
        $data['title'] = "Discussion";
        $data['idvendeur'] = $id;
        $this->load->view('templates/header', $data);
        $this->load->view('pages/mychat');
        $this->load->view('templates/footer');
        // }
    }
<<<<<<< HEAD

    public  function signaler_get()
    {

      
        // $data['article'] = $this->article_model->get_article_by_id($idarticle);
        // var_dump($data);
        // return;
         $this->load->view('templates/header');
        $this->load->view('pages/signaler');
        $this->load->view('templates/footer');
    }
=======
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('Vendeur/index');
    }
}
