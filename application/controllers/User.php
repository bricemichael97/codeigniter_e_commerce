<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This class deals with all admin interactions with the application
 *
 * @package         UserPackage
 * @category        Controller
 * @author          @Melo
 * @link            https://gitlab.com/philippe1997
 */
class User extends REST_Controller
{

    public $adresse = "https://www.squares.seeds.cm/";
    public $repertoire_image_produit = "image_produit";
    public $repertoire_profil = "profil";
    public $repertoire_couverture_profil = "couverture_profil";
    public $url = "https://www.squares.cm/";
    public $reponse = "";
    public $token_border = "BORDURE_TOKEN";

    public $footer = "";

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->reponse = new stdClass();

        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }

    /**
     * @SWG\Post(
     *     path="/User/user",
     *     tags={"user"},
     *     operationId="addUser",
     *     summary="Add a new user",
     *     description="For more details on content consult the user model",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/ApiResponse")
     *     )
     *  $article=  $this->user_model->add_article($data);
    if ($article) {

    $this->reponse->body = (object) $article;
    $this->reponse->status = 'success';
    $this->response($this->reponse, REST_Controller::HTTP_OK);
    } else {
    $this->reponse->body = (object) $data;
    $this->reponse->status = 'failed';
    $this->reponse->code = '101';
    $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
     * )
     */
    public function user_post()
    {
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }
        $data = $this->post();


        //        if (!$this->user_model->exist_user($data['email_user'])) {

        $user = $this->user_model->add_user($data);

        if ($user) {

            $this->reponse->body = (object) $user;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = (object) $data;
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }


        //        } else {
        //            $this->reponse->body = (object) $data;
        //            $this->reponse->status = 'failed';
        //            $this->reponse->code = '102';
        //            $this->response($this->reponse, REST_Controller::HTTP_OK);
        //        }

    }
    public function categorie_post()
    {
    
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }
        $data = $this->post();

         
          
     

        $user = $this->categorie_model->add_categorie($data); 

        if ($user) {

            $this->reponse->body = (object) $user;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = (object) $data;
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function article_post()
    {
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }


        /* $config = [
            [
                'field' => 'nomarticle',
                'label' => 'nomarticle',
                'name' => 'nomarticle',
                'rules' => 'required|min_length[3]',
                'errors' => [
                    'required' => 'We need both username and password',
                    'min_length' => 'Minimum Username length is 3 characters',

                ],
            ],
            [
                'field' => 'quantitearticle',
                'name' => 'quantitearticle',
                'label' => 'quantitearticle',
                'rules' => 'required|min_length[6]',
                'errors' => [
                    'required' => 'You must provide a Password.',
                    'min_length' => 'Minimum Password length is 6 characters',
                ],
            ],
        ];*/
        $data = $this->post();
        //        $this->load->library('form_validation');
        //$this->form_validation->set_data($data);
        //$this->form_validation->set_rules($config);

        //        print_r($this->form_validation);
        //        return;
       /* if ($this->form_validation->run() == TRUE) {
            print_r($this->form_validation->error_array());
            echo "ERROR!!";
        } else {*/
            $article = $this->user_model->add_article($data);
             
            if ($article ) {
                $this->reponse->body = (object) $article;
                $this->reponse->status = 'success';
                $this->response($this->reponse, REST_Controller::HTTP_OK);
            } else {
                $this->reponse->body = (object) $data;
                $this->reponse->status = 'failed';
                $this->reponse->code = '101';
                $this->response($this->reponse, REST_Controller::HTTP_OK);
            }


            //        } else {
            //            $this->reponse->body = (object) $data;
            //            $this->reponse->status = 'failed';
            //            $this->reponse->code = '102';
            //            $this->response($this->reponse, REST_Controller::HTTP_OK);
            //        }

      /*  }*/
    }
    //        if (!$this->user_model->exist_user($data['email_user'])) {


    public function evenement_post()
    {
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }
        $data = $this->post();


        //        if (!$this->user_model->exist_user($data['email_user'])) {

        $evenement = $this->user_model->add_evenement($data);

        if ($evenement) {

            $this->reponse->body = (object) $evenement;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = (object) $data;
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }


        //        } else {
        //            $this->reponse->body = (object) $data;
        //            $this->reponse->status = 'failed';
        //            $this->reponse->code = '102';
        //            $this->response($this->reponse, REST_Controller::HTTP_OK);
        //        }

    }


    public function update_pass_admin_post()
    {
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }
        $data = $this->post();

        // if (!isset($data['date_creation'])) {
        $data['date_update'] = date("Y-m-d");
        // }
        if ($data['password'] === null || $data['password'] === 'null' || $data['password'] === 'NULL') {
            $this->reponse->body = 'Veuillez inserer un mot de passe';
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $data['password'] = md5($data['password']);
            $data['ancien_pass'] = md5($data['ancien_pass']);
        }
        if ($data['ancien_pass'] === null || $data['ancien_pass'] === 'null' || $data['ancien_pass'] === 'NULL') {
            $this->reponse->body = "Ancien mot de passe incompatible";
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $data['ancien_pass'] = md5($data['ancien_pass']);
        }
        $user_db = (array) $this->user_model->get_user_by_id($data['id_utilisateur']);
        if ($data['ancien_pass'] !== $user_db['password']) {
            unset($data['ancien_pass']);
            $user = $this->user_model->update_user($data);
            if ($user) {
                $this->reponse->body = $user;
                $this->reponse->status = 'success';
                $this->response($this->reponse, REST_Controller::HTTP_OK);
            } else {
                $this->reponse->body = $user;
                $this->reponse->status = 'failed';
                $this->response($this->reponse, REST_Controller::HTTP_OK);
            }
        } else {
            $this->reponse->body = "Ancien mot de passe incompatible";
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
        $data['actif'] = '1';
    }

    /**
     * @SWG\Post(
     *     path="/User/update_user",
     *     tags={"user"},
     *     operationId="updateUser",
     *     summary="Update a user",
     *     description="For more details on content consult the user model",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/ApiResponse")
     *     )
     * )
     */
    public function update_user_post()
    {

        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $data = $this->post();
        if (!isset($data['date_update_user'])) {
            $data['date_update_user'] = date("Y-m-d H:i:s");
        }
        if (!isset($data['id_utilisateur'])) {
            $this->reponse->body = "Vous devez renvoyer envoyer l'id User";
            $this->reponse->status = 'failed';
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
        if (isset($data['password'])) {
            $data['password'] = md5($data['password']);
        }

        if ($_FILES) {
            $user_db = (array) $this->user_model->get_user_by_id($data['id_utilisateur']);
            if (isset($_FILES["profil"])) {
                if ($user_db["profil"] != "" && $user_db["profil"] != null && $user_db["profil"] != "null") {
                    $lien = str_replace(base_url(), "", $user_db["profil"]);
                    if (file_exists($lien)) {
                        unlink($lien);
                    }
                }
                $_FILES['profil']["name"] = "profil" . time() . "." . $ext = pathinfo($_FILES["profil"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["profil"], $this->repertoire_profil);
                $data["profil"] = base_url() . $chemin1;
            }
        }

        $user = $this->user_model->update_user($data);
        if ($user) {
            $this->reponse->body = $user;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $user;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    /**
     * @SWG\Post(
     *     path="/User/login",
     *     tags={"user"},
     *     operationId="loginOwner",
     *     summary="Login of an owner",
     *     description="For more details on content consult the user model",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="body",
     *         description="Email of the owner",
     *         required=true,
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="body",
     *         description="Password of the owner",
     *         required=true,
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/ApiResponse")
     *     )
     * )
     */
    public function login_post()
    {
        $data = $this->post();
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $data['password_user'] = md5($data['password_user']);

        $user = $this->user_model->login_user($data);
        if ($user) {
            // if ($user->is_deleted === '1' || $user->actif === '0') {
            //     $this->reponse->code = '103';
            //     $this->reponse->status = 'failed';
            //     $this->reponse->body = (object) $data;
            //     $this->response($this->reponse, REST_Controller::HTTP_OK);
            // } else {
            $this->reponse->body = $user;
            $this->reponse->code = '100';
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            // }

        } else {
            $this->reponse->body = (object) $data;
            $this->reponse->status = 'failed';
            $this->reponse->code = '104';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    /**
     * @SWG\Get(
     *     path="/User/users",
     *     summary="Get all owners",
     *     description="Get all owners",
     *     operationId="getAllOwners",
     *     produces={"application/json"},
     *     tags={"user"},
     *   @SWG\Parameter(
     *     name="debut",
     *     in="query",
     *     description="Page number (Offset - Optional)",
     *     type="integer",
     *     format="int64",
     *     default=0
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit amount of record (Optional)",
     *     type="integer",
     *     format="int64",
     *     default=50
     *   ),
     *    @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/ApiResponse")
     *     )
     * )
     */
    public function users_get()
    {
        $result = $this->user_model->get_users($this->get('debut'), $this->get('limit'));
        $this->reponse->body = $result;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }

    /**
     * @SWG\Get(
     *     path="/User/user/id_utilisateur/{id_utilisateur}",
     *     summary="Find an owner by ID",
     *     description="Returns a single owner",
     *     operationId="getOwnerById",
     *     tags={"user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of owner to return",
     *         in="path",
     *         name="id_utilisateur",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/ApiResponse")
     *     )
     * )
     */
    public function user_get()
    {
        $data = $this->get();
        if (isset($data['id_utilisateur'])) {
            $result = $this->user_model->get_user_by_id($this->get('id_utilisateur'));
            $this->reponse->body = $result;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = new stdClass();
            $this->reponse->status = 'failed';
            $this->reponse->message = "Vous devez envoyer l'id de l'utilisateur en question";
            $this->reponse->code = '101';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
}
