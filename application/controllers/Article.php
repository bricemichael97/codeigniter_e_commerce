<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This class deals with all admin interactions with the application
 *
 * @package         ArticlePackage
 * @category        Controller
 * @author          @Melo
 * @link            https://gitlab.com/philippe1997
 */
class Article extends REST_Controller
{

    public $repertoire_couverture_article = "article";
    public $repertoire_ressouce_article = "referenceproduit";

    public $reponse = "";

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->reponse = new stdClass();
        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }
    public function index_get($page = 'produit')
    {

        $this->view($page);
    }
    public function view($page = "produit")
    {
        $data = array();
        $flow = $_SESSION['idvendeur'];
        $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
        /* $data['evenements'] = $this->evenement_model->fetch_data(); */
        // $data['evenements'] = $this->evenement_model->get_evenements(0,3);
        // $data['resource_articles'] = $this->article_model->fetch_resource();
        $data['categories'] = $this->categorie_model->fetch_data();
        // var_dump($data['categories']);
        // return;
        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
    }
    public function handle_image_post()
    {
        if ($_FILES) {
            if (isset($_FILES["upload"])) {
                /*** upload de l'image du produit**/
                $_FILES['upload']["name"] = "ressouce_article" . time() . "." . $ext = pathinfo($_FILES["ressouce_article"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["ressouce_article"], $this->repertoire_ressouce_article);
                $this->reponse->url = base_url() . $chemin1;
            } else {
                $this->reponse->uploaded = false;
                $this->reponse->url = 'Label your file upload please';
            }
        } else {
            $this->reponse->uploaded = false;
            $this->reponse->url = 'No file';
        }
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
    public function article_post($page = "produit")
    {
        $data = $this->post();

        
        // var_dump($data);
        $resource = array();
        if ($_FILES) {

            if (isset($_FILES["imagearticle"])) {

                $_FILES['imagearticle']["name"] = "imagearticle" . time() . "." . $ext = pathinfo($_FILES["imagearticle"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagearticle"], $this->repertoire_couverture_article);
                $data["imagearticle"] = base_url() . $chemin1;
            }
            if (isset($_FILES["ressource_article"])) {
                $length = count($_FILES['ressource_article']['name']);

                for ($i = 0; $i < $length; $i++) {
                    # code...
                    /*** upload de l'image du produit**/
                    // $=$_FILES["imagearticle"];
                    $_FILES['ressource_article']["name"][$i] = "ressource_article" . time() . $i . "." . $ext = pathinfo($_FILES["ressource_article"]["name"][$i], PATHINFO_EXTENSION);
                    $image = [];
                    $image['name'] = $_FILES['ressource_article']["name"][$i];
                    $image['tmp_name'] = $_FILES['ressource_article']["tmp_name"][$i];
                    $chemin1 = $this->file->uploader_image($image, $this->repertoire_couverture_article);
                    // $data["imagearticle"] = base_url() . $chemin1;
                    array_push($resource, base_url() . $chemin1);
                }
            }
        }
        
        $user= $this->categorie_model->get_categorie_bys_id($data['id_categorie']);
        $users=(array)$user;
        $data['libelle_categorie'] = $users['libelle_categorie'];
        $data['date_ajout'] = time();
        // $data['date_ajout'] = date("Y-m-d H:i:s");
        $show = $data['date_ajout'];
        $data['date_ajout'] = date("m-d-Y h:i:s ", $show); 
        $data['idvendeur'] = $_SESSION['idvendeur'];
        $data['telephonevendeur'] = $_SESSION['telephonevendeur'];
        $data['nomvendeur'] = $_SESSION['nomvendeur'];
       
        // $data['nomvendeur'] = $_SESSION['date_ajout'];

        // if ($data['libelle_categorie'] == $_SESSION['Télephone et Tablettes']) { 
        //     $data['id_categorie'] = 1;
        // }else{
        // if ($data['libelle_categorie'] == $_SESSION['Informatiques et Multimédias
        // ']) {
        //     $data['id_categorie'] = 2;
        // }else{
        // if ($data['libelle_categorie'] == $_SESSION['Véhicules']) {
        //     $data['id_categorie'] = 3;
        // }else{
        // if ($data['libelle_categorie'] == $_SESSION['Loisirs']) {
        //     $data['id_categorie'] = 4;
        // }else{
        // if ($data['libelle_categorie'] == $_SESSION['Mode et Vêtements']) {
        //     $data['id_categorie'] = 5;
        // }else{
        // if ($data['libelle_categorie'] == $_SESSION['Immobilier']) {
        //     $data['id_categorie'] = 6;
        // }else{
        //     if ($data['libelle_categorie'] == $_SESSION['La cave']) {
        //         $data['id_categorie'] = 7;
        // }else{
        //  if ($data['libelle_categorie'] == $_SESSION['Service']) {
        //             $data['id_categorie'] = 8;
        //   }else{
        //   if ($data['libelle_categorie'] == $_SESSION['Electroménager']) {
        //                 $data['id_categorie'] = 11;
        //  }else{
        //  if ($data['libelle_categorie'] == $_SESSION['Beauté et bien être']) {
        //                     $data['id_categorie'] = 12;
        //   }else{
        //   }   
      
        // var_dump($data);
        // return;

        // $data['libelle_categorie'] = $_SESSION['libelle_categorie'];
        $result = $this->article_model->add_article($data, $resource);

        if ($result) {
            // $this->notifyUsers('200', $user); 

            $flow = $_SESSION['idvendeur'];
            $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
            $data['categories'] = $this->categorie_model->fetch_data();
            $data['result'] = $result;
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }


    public function update_article_post($page = "produit")
    {
        if (!$this->post()) {
            $this->empty_formatter();
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $data = $this->post();

        if ($_FILES) {
            $user_db = (array) $this->user_model->get_article_by_id($data['idarticle']);
            if (isset($_FILES["imagearticle"])) {
                if ($user_db["imagearticle"] != "" && $user_db["imagearticle"] != null && $user_db["imagearticle"] != "null") {
                    $lien = str_replace(base_url(), "", $user_db["imagearticle"]);
                    if (file_exists($lien)) {
                        unlink($lien);
                    }
                }
                $_FILES['imagearticle']["name"] = "imagearticle" . time() . "." . $ext = pathinfo($_FILES["imagearticle"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagearticle"], $this->repertoire_couverture_article);
                $data["imagearticle"] = base_url() . $chemin1;
            }
        }
        $data['date_ajout'] = date("Y-m-d H:i:s");
        // $data['idvendeur'] = $_SESSION['idvendeur'];
         $user= $this->categorie_model->get_categorie_bys_id($data['id_categorie']);
        $users=(array)$user;
        $data['libelle_categorie'] = $users['libelle_categorie'];
        $result = $this->article_model->update_article($data);
        // var_dump($result);
        // return;
        if ($result) {
            $flow = $_SESSION['idvendeur'];
            $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
            $data['result'] = $result;

            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function article_get($idarticle)
    {
        // $id = $this->uri->segment(3); 
        $page = "produit";
        if ($idarticle) {
            $this->load->model("article_model");
            $delet = $this->article_model->delete_article($idarticle);
            if ($delet) {
                $flow = $_SESSION['idvendeur'];
                $data['articles'] = $this->article_model->get_article_fromvendeur($flow);
                $data['categories'] = $this->categorie_model->fetch_data();

                $this->load->view('templates/header');
                $this->load->view('pages/' . $page, $data);
                $this->load->view('templates/footer');
                /*$this->reponse->status = 'success';
                $this->response($this->reponse, REST_Controller::HTTP_OK);*/
            } else {
                $this->reponse->status = 'not found';
                $this->response($this->reponse, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->reponse->status = 'not found';
        }
    }

    /* public function article_get()
    {

        $result = $this->article_model->get_article_by_id($this->get('id_article'));

        $this->reponse->body = $result;

        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }*/

    /* public function articles_get()
    {
        $result = $this->article_model->get_articles($this->get('debut'), $this->get('limit'));

        $this->reponse->body = $result;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
*/
    public function search_post()
    {
        $data = array();
        $page = "produit";
        $data = $this->post();
        $new  = $data['key'];
        $data['articles'] = $this->article_model->set($new);

        // $data['store']
        if ($data) {
            // $this->notifyUsers('200', $user);

            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function articles_get()
    {
        $result = $this->article_model->get_articles($this->get('debut'), $this->get('limit'));

        $this->reponse->body = $result;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }

    public function articles_user_get()
    {
    }
    public function add_commenaire_post()
    {
        $comment = $this->post();
        // var_dump($comment);
        // return;
        $comment['date_ajout_commentaire'] = time();
        $result = $this->article_model->add_commentaire($comment);
        $data = [];
        $idarticle = $comment['id_article'];
        $data = array();
        $data['article'] = $this->article_model->get_article_by_id($idarticle);
        $data['idarticle'] = $idarticle;
        $data['commentaires'] = $this->article_model->get_product_comments($idarticle);
        $data['resource_articles'] = $this->article_model->get_resource_by_id($idarticle);
        $data['articles'] = $this->categorie_model->get_categorie_products($data['article']->id_categorie);
        $data['share_link'] = current_url();
        $data['vendeur'] = $this->vendeur_model->get_vendeur_by_id($data['article']->idvendeur);
        $this->load->view('templates/header');
        $this->load->view('pages/article', $data);
        $this->load->view('templates/footer');
    }
    public function add_favoris_post()
    {
        $favoris = $this->post();
        $result = $this->vendeur_model->add_favoris($favoris);
        $data = [];
        $idarticle = $data['idarticle'];
        $data = array();
        $data['article'] = $this->article_model->get_article_by_id($idarticle);
        $data['idarticle'] = $idarticle;
        $data['commentaires'] = $this->article_model->get_product_comments($idarticle);
        $data['resource_articles'] = $this->article_model->get_resource_by_id($idarticle);
        $data['articles'] = $this->categorie_model->get_categorie_products($data['article']->id_categorie);
        $data['share_link'] = current_url();
        $data['vendeur'] = $this->vendeur_model->get_vendeur_by_id($data['article']->idvendeur);
        $this->load->view('templates/header');
        $this->load->view('pages/article', $data);
        $this->load->view('templates/footer');
    }
    public function like_get($idarticle)
    {
        // $favoris = $this->post();
        $result = $this->vendeur_model->like($idarticle);
        $data = [];
        // $idarticle = $data['idarticle'];
        $data = array();
        $data['article'] = $this->article_model->get_article_by_id($idarticle);
        $data['idarticle'] = $idarticle;
        $data['commentaires'] = $this->article_model->get_product_comments($idarticle);
        $data['resource_articles'] = $this->article_model->get_resource_by_id($idarticle);
        $data['articles'] = $this->categorie_model->get_categorie_products($data['article']->id_categorie);
        $data['share_link'] = current_url();
        $data['vendeur'] = $this->vendeur_model->get_vendeur_by_id($data['article']->idvendeur);
        $this->load->view('templates/header');
        $this->load->view('pages/article', $data);
        $this->load->view('templates/footer');
    }
    public function searchacc_post()
    {
        $data = array();
        $page = "categorie_show";
        $data = $this->post();
        $new  = $data['key'];
        $data['articles'] = $this->article_model->set($new);




        // $data['store']
        if ($data) {
            // $this->notifyUsers('200', $user);
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function empty_formatter()
    {
        $this->reponse->code = "104";
        $this->reponse->body = new stdClass();
        $this->reponse->message = "Aucune donnée trouvé";
        $this->reponse->status = "failed";
    }
}
