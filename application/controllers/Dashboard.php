<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->reponse = new stdClass();
        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }


    public function index()
    {
        $data = array();

        if ($_SESSION['type_user'] == '2') {
            $data['articles'] = $this->vendeur_model->vendeur_favoris($_SESSION['idvendeur']);
        }
        $this->load->view('templates/header');
        $this->load->view('pages/dashboard', $data);
        $this->load->view('templates/footer');
    }
}
