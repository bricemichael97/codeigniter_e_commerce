<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This class deals with all admin interactions with the application
 *
 * @package         ArticlePackage
 * @category        Controller
 * @author          @Melo
 * @link            https://gitlab.com/philippe1997
 */
class Categorie extends REST_Controller
{

    public $repertoire_couverture_categorie = "categorie";
    public $repertoire_ressouce_article = "referenceproduit";

    public $reponse = "";

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->reponse = new stdClass();
        $this->reponse->body = "";
        $this->reponse->code = "100";
        $this->reponse->status = "success";
        $this->reponse->message = "";
    }
    public function index_get($page = 'categorie_show')
    {

        $this->view($page);
    }
    public function view($page = "categorie_show")
    {
        $data = array();

        // $data['categories'] = $this->categorie_model->fetch_data();
        /* $data['evenements'] = $this->evenement_model->fetch_data(); */
        // $data['evenements'] = $this->evenement_model->get_evenements(0,3);
        // var_dump( $_SESSION);
        // return;
        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
    }

    public function handle_image_post()
    {
        if ($_FILES) {
            if (isset($_FILES["upload"])) {
                /*** upload de l'image du produit**/
                $_FILES['upload']["name"] = "ressouce_article" . time() . "." . $ext = pathinfo($_FILES["ressouce_article"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["ressouce_article"], $this->repertoire_ressouce_article);
                $this->reponse->url = base_url() . $chemin1;
            } else {
                $this->reponse->uploaded = false;
                $this->reponse->url = 'Label your file upload please';
            }
        } else {
            $this->reponse->uploaded = false;
            $this->reponse->url = 'No file';
        }
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
    public function categorie_post()
    {
        $data = $this->post();
        if (!$this->post()) {
            $this->reponse->code = "104";
            $this->reponse->body = new stdClass();
            $this->reponse->message = "Aucune donnée trouvé";
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $article = $this->categorie_model->add_categorie($data);
        if ($article) {
            // $this->notifyUsers('200', $user);
            $this->reponse->body = $article;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $article;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function category_post()
    {
        $data = $this->post();

        if ($_FILES) {
            if (isset($_FILES["imagecategorie"])) {
                /*** upload de l'image du produit**/
                $_FILES['imagecategorie']["name"] = "imagecategorie" . time() . "." . $ext = pathinfo($_FILES["imagecategorie"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagecategorie"], $this->repertoire_couverture_categorie);
                $data["imagecategorie"] = base_url() . $chemin1;
            }
        }
        $user = $this->categorie_model->add_categorie($data);
        if ($user) {
            // $this->notifyUsers('200', $user);
            $this->reponse->body = $user;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $user;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function update_categorie_post()
    {
        if (!$this->post()) {
            $this->empty_formatter();
            $this->response($this->reponse, REST_Controller::HTTP_OK);
            return;
        }

        $data = $this->post();

        if ($_FILES) {
            $user_db = (array) $this->categorie_model->get_categorie_by_id($data['id_categorie']);

            if (isset($_FILES["imagecategorie"])) {
                if ($user_db["imagearticle"] != "" && $user_db["imagecategorie"] != null && $user_db["imagecategorie"] != "null") {
                    $lien = str_replace(base_url(), "", $user_db["imagecategorie"]);
                    if (file_exists($lien)) {
                        unlink($lien);
                    }
                }
                $_FILES['imagecategorie']["name"] = "imagecategorie" . time() . "." . $ext = pathinfo($_FILES["imagecategorie"]["name"], PATHINFO_EXTENSION);
                $chemin1 = $this->file->uploader_image($_FILES["imagecategorie"], $this->repertoire_couverture_categorie);
                $data["imagecategorie"] = base_url() . $chemin1;
            }
        }
        /* $data['date_update_article'] = date("Y-m-d H:i:s");*/

        $result = $this->categorie_model->update_categorie($data);
        if ($result) {
            $this->reponse->body = $result;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        } else {
            $this->reponse->body = $result;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function delete_article_post()
    {

    }

    public function cat_get($id_categorie)
    {
        $data = array();
        $page = "categorie_show";
        $data['articles'] = $this->categorie_model->get_articles_by_idcat($id_categorie); 
        $data['categorie'] = $this->categorie_model->get_categorie_by_id($id_categorie);

        if ($data) { 
             
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
            // $this->load->view('templates/header');
            // $this->load->view('pages/' . $page, $result);
            // $this->load->view('templates/footer');
        } else { 
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function cat_data_get($id_categorie)
    {
        $data = array();
        $page = "categorie_show";
        $data['articles'] = $this->categorie_model->get_articles_by_idcat($id_categorie);  
        if ($data) { 
             
            $this->reponse->body = $data;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK); 
       
            // $this->load->view('templates/header');
            // $this->load->view('pages/' . $page, $result);
            // $this->load->view('templates/footer');
        } else { 
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function categori_get()
    {
        $result = $this->categorie_model->fetch_data();

        $this->reponse->body = $result;
        $this->reponse->status = 'success';
        $this->response($this->reponse, REST_Controller::HTTP_OK);
    }
    public function search_post()
    {
        $data = array();
        $page = "categorie_show";
        $data = $this->post();
        $new  = $data['key'];
        $data['articles'] = $this->categorie_model->set($new);

        // $data['store']
        if ($data) {
            // $this->notifyUsers('200', $user);
            $this->load->view('templates/header');
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer');
        } else {
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }
    public function categories_articles_get($id_categorie)
    {
        $data = array();
        $data['articles'] = $this->categorie_model->get_nombrearticle($id_categorie); 
        
       $play= $data['articles'];


        if ($play) { 
             
            $this->reponse->body = $play;
            $this->reponse->status = 'success';
            $this->response($this->reponse, REST_Controller::HTTP_OK); 
       
            // $this->load->view('templates/header');
            // $this->load->view('pages/' . $page, $result);
            // $this->load->view('templates/footer');
        } else { 
            $this->reponse->body = $data;
            $this->reponse->status = 'failed';
            $this->response($this->reponse, REST_Controller::HTTP_OK);
        }
    }

    public function empty_formatter()
    {
        $this->reponse->code = "104";
        $this->reponse->body = new stdClass();
        $this->reponse->message = "Aucune donnée trouvé";
        $this->reponse->status = "failed";
    }
}
