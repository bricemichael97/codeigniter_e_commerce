<?php defined('BASEPATH') or exit('No direct script access allowed');


class article_model extends CI_Model
{

    public $table_name_article = "article";
    public $table_name_resource = 'resource_article';
    public function get_articless()
    {
        $query = $this->db->select('*')->from("article")->order_by('idarticle', 'asc');
        $this->db->join('categorie', 'categorie.id_categorie=article.idcategorie');
        // $this->db->join('categorie','categorie.id_categorie=article.idcategorie');
        return $this->db->get()->result();
    }
    public function get_articles($debut, $limit)
    {
        $this->db->select('*')->from("article")->limit($limit, $debut)->order_by('idarticle', 'asc');
        // $this->db->join('categorie','categorie.id_categorie=article.idcategorie');
        return $this->db->get()->result();
    }

    public function add_article($data, $resources)
    {
        $result = $this->db->insert($this->table_name_article, $data);
        $data['idarticle'] = $this->db->insert_id();

        foreach ($resources as $results) :
            $top = array();
            $top['idarticle'] = $data['idarticle'];
            $top['lien_image'] = $results;
            $this->db->insert($this->table_name_resource, $top);
        endforeach;
        return $data;
    }
    function delete_article($idarticle)
    {
        $this->db->where("idarticle", $idarticle);
        $delete = $this->db->delete("article");

        return $delete ? true : false;
    }
    public function get_article_by_id($idarticle)
    {
        return $this->db->select('*')->where('idarticle', $idarticle)->get('article')->row();
    }
    public function get_ar_by_id($idarticle)
    {
        return $this->db->select('idarticle')->where('idarticle', $idarticle)->get('article')->row();
    }
    public function update_article($data)
    {
        $this->db->where('idarticle', $data['idarticle']);
        $this->db->update($this->table_name_article, $data);

        return $this->get_article_by_id($data['idarticle']);
    }
    public function get_article_fromvendeur($idvendeur)
    {
        return  $this->db->select('*')->from("article")->where('idvendeur', $idvendeur)->get()->result();
    }
    public function get_nombrearticle($idvendeur)
    {
        return  $this->db->count_all_results('article')->where('idvendeur', $idvendeur)->get()->result();
    }
    public function set($key)
    {
        $this->db->select('*');
        $this->db->like('nomarticle', $key);
        $this->db->or_like('prixarticle', $key);
        return  $this->db->get('article')->result();
    }
    function fetch_resource()
    {

        $query = $this->db->select('*')->from("article")->get()->result();
        return $query;
    }
    public function get_resource_by_id($idarticle)
    {
        return $this->db->select('*')->where('idarticle', $idarticle)->get('resource_article')->result();
    }

    public function add_commentaire($data)
    {


        $this->db->insert('commentaire', $data);
        $data['id_commentaire'] = $this->db->insert_id();

        return $data;
    }
    public function get_product_comments($data, $debut = "0", $limit = "50")
    {
        $this->db->select('*')->from('commentaire');
        $this->db->where('id_article', $data);
        return $this->db->limit($limit, $debut)->get()->result();
    }
    public function get_pages()
    {
        return $this->db->select('*')->from("article")->order_by('date_ajout', 'asc')->get()->result();;
    }
    

}
