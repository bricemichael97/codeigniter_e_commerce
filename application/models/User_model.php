<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This class deals with all admin interactions with the application
 *
 * @package         UserPackage
 * @category        Model
 * @author          @Melo
 * @link            https://github.com/philippe1997
 */
class User_model extends CI_Model
{
    public $table_name = "client";
    public $table_name_article = "article";
    public $table_name_evenement = "evenement";


    public $table_name_phone = "telephone";

    public function add_user($data)
    {

        $result = $this->db->insert($this->table_name, $data);

        $data['idclient'] = $this->db->insert_id();
        return $data;
    }
    public function add_article($data)
    {
        $result = $this->db->insert($this->table_name_article, $data);

        $data['idarticle'] = $this->db->insert_id();
        return $data;
    }

    public function add_evenement($data)
    {
        $result = $this->db->insert($this->table_name_evenement, $data);

        $data['idevenement'] = $this->db->insert_id();
        return $data;
    }


    public function get_user_by_id($id)
    {
        return $this->db->select('*')->where('id_user', $id)->get('user')->row();
    }

    public function get_article_by_id($id)
    {
        return $this->db->select('*')->where('idarticle', $id)->get('article')->row();
    }

    public function exist_user($email)
    {
        $result = $this->db->select('*')->where('email_user', $email)->get('user')->row();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }


    public function get_users($debut = 0, $limit = 50)
    {
        return $this->db->select('*')->limit($limit, $debut)->from('utilisateur')->get()->result();
    }

    public function get_articles($debut , $limit )
    {
        return $this->db->select('*')->from($this->table_name_article)->limit($limit, $debut)->order_by('idarticle','asc')->get()->result();
    }


    public function login_user($data)
    {
        $result = $this->db->select('*')->where('email_user', $data["email_user"])->where('password_user', $data["password_user"])->get($this->table_name);
        $result = $result->row();

        return $result;

    }

    public function update_user($data)
    {
        $this->db->where('id_user', $data['id_user']);
        $this->db->update($this->table_name, $data);

        return $this->get_user_by_id($data['id_user']);
    }
    public function update_article($data)
    {
        $this->db->where('idarticle', $data['idarticle']);
        $this->db->update($this->table_name_article, $data);

        return $this->get_article_by_id($data['idarticle']);
    }
}
