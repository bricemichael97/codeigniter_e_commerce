<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This class deals with all admin interactions with the application
 *
 * @package         UserPackage
 * @category        Model
 * @author          @Melo
 * @link            https://github.com/philippe1997
 */
class vendeur_model extends CI_Model
{
    public $table_name_vendeur = "vendeur";




    public function add_vendeur($data)
    {

        $result = $this->db->insert($this->table_name_vendeur, $data);

        $data['idvendeur'] = $this->db->insert_id();
        return $this->get_vendeur_by_id($data['idvendeur']);
    }
    public function add_article($data)
    {
        $result = $this->db->insert($this->table_name_article, $data);

        $data['idarticle'] = $this->db->insert_id();
        return $data;
    }


    public function get_vendeur_by_id($id)
    {
        return $this->db->select('*')->where('idvendeur', $id)->get('vendeur')->row();
    }


    public function exist_vendeur($email)
    {
        $result = $this->db->select('*')->where('emailvendeur', $email)->get('email')->row();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function get_vendeurs($debut = 0, $limit = 50)
    {
        return $this->db->select('*')->limit($limit, $debut)->from('vendeur')->get()->result();
    }
    public function get_vendeurss()
    {
        $query = $this->db->select('*')->from("vendeur")->get()->result();
        return $query;
    }
    public function get_articles($debut, $limit)
    {
        return $this->db->select('*')->from($this->table_name_vendeur)->limit($limit, $debut)->order_by('idvendeur', 'asc')->get()->result();
    }


    public function login_vendeur($data)
    {
        $result = $this->db->select('*')->where('emailvendeur', $data["emailvendeur"])->where('passwordvendeur', $data["passwordvendeur"])->get($this->table_name_vendeur);
        $result = $result->row();

        return $result;
    }

    public function update_vendeur($data)
    {
        $this->db->where('idvendeur', $data['idvendeur']);
        $this->db->update($this->table_name_vendeur, $data);

        return $this->get_vendeur_by_id($data['idvendeur']);
    }

    public function vendeur_favoris($data)
    {
        $this->db->select('article.*')->from('article')->where('article.idvendeur', $data);

        $this->db->join('r_avoir_favoris', 'r_avoir_favoris.idarticle=article.idarticle');

        return $this->db->get()->result();
    }

    public function add_favoris($data)
    {
        $result = $this->db->insert('r_avoir_favoris', $data);
        return true;
    }
    public function like($idarticle)
    {
        $increment = 1;
        $this->db->where('idarticle', $idarticle);
        $this->db->set('nombrenote', 'nombrenote+' . $increment, false);
        $this->db->update('article');

        return $idarticle;
    }
}
