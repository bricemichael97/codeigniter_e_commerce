<?php

/**
 * @SWG\Definition(type="object")
 */
class ApiResponse
{

    /**
     * @SWG\Property(format="int32",enum={"100","102","103","104","105","106","107","108","109"},description="100 : Succès de l’opération, 101 : Echec de l’opération, 102 : Données existante, 103 : Accès refusé (compte bloqué), 104 : Aucune donnée trouvée, 105 : Mail envoyé, compte désactivé,veuillez activez le compte, 106 : Périphérique non valide, 107 : Mot de passe invalide, 108 : Code d’activation expiré, il est à renouveler, 109 : Données manquantes l'ors de l'envoie, 110:Erreur inconnu")
     * @var int
     */
    public $code = 200;

    /**
     * @SWG\Property
     * @var bool
     */
    public $status = true;

    /**
     * @SWG\Property(description="This attribute will generally be an object for a POST and an array for a GET <b>BUT</b> it can be in particular situation be an object in a GET")
     * @var object
     */
    public $data;

    /**
     * @SWG\Property
     * @var string
     */
    public $message;

/*
Les valeurs de code
100    Succès de l’opération
101    Echec de l’opération
102    Données existante
103    Accès refusé (compte bloqué)
104    Aucune donnée trouvée
105    Mail envoyé, compte désactivé,veuillez activez le compte
106    Périphérique non valide
107    Mot de passe invalide
108    Code d’activation expiré, il est à renouveler
 */
}