<?php defined('BASEPATH') or exit('No direct script access allowed');


class Categorie_model extends CI_Model
{

    public $table_name_categorie = "categorie";

    public function get_categorie($debut, $limit)
    {
        return $this->db->select('*')->from($this->table_name_categorie)->limit($limit, $debut)->order_by('id_categorie', 'asc')->get()->result();
    }
    public function get_categorie_products($id_categorie)
    {
        $query =  $this->db->select('art.idarticle,art.nomarticle,art.imagearticle,cat.id_categorie,cat.imagecategorie,art.idvendeur,art.libelle_categorie,art.prixarticle,art.nomvendeur,art.date_ajout,')
            ->from('article as art')
            ->join('categorie as cat', 'cat.id_categorie=art.id_categorie')
            ->where('art.id_categorie', $id_categorie)
            ->get();
        return $query->result();
        
    }
    
    public function get_categorie_bys_id($id_categorie)
    {
        return $this->db->select('*')->from('categorie')->where('id_categorie', $id_categorie)->get()->row();
        return $this->db->select('*')->from('article')->where('id_categorie', $id_categorie)->get()->result();
    }
    public function add_categorie($data)
    {
        $result = $this->db->insert($this->table_name_categorie, $data);

        $data['id_categorie'] = $this->db->insert_id();
        return $data;
    }
    function fetch_data()
    {

        $query = $this->db->select('*')->from("categorie")->get()->result();

        foreach ($query as $even) :
            $even->nombrearticle = $this->db->where('id_categorie', $even->id_categorie)->from('article')->count_all_results();
        endforeach;
        return $query;
    }
    public function get_categorie_by_id($idcategorie)
    {
        $this->db->select('*')->from("categorie")->where('id_categorie', $idcategorie);
        // $this->db->join('categorie','categorie.id_categorie=article.idcategorie');
        return $this->db->get()->row();
    }
    public function get_nombrearticle($idcategorie)
    {
        return  $this->db->where('id_categorie', $idcategorie)->from('article')->count_all_results();
    }
    public function get_articles_by_idcat($idcategorie)
    {
        return $this->db->select('*')->where('id_categorie', $idcategorie)->get('article')->result();
    }

    public function set($key)
    {
        $this->db->select('*');
        $this->db->like('libelle_categorie', $key);
        return  $this->db->get('article')->result();
    }
    public function update_categorie($data)
    {
        $this->db->where('id_categorie', $data['id_categorie']);
        $this->db->update($this->table_name_categorie, $data);

        return $this->get_categorie_by_id($data['id_categorie']);
    }
}
