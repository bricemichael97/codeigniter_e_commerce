<?php defined('BASEPATH') or exit('No direct script access allowed');

class Evenement_model extends CI_Model
{
    public $table_name_evenement = "evenement";

    public function add_evenement($data)
    {
        $articles = $data['articles'];
        unset($data['articles']);
        $result = $this->db->insert($this->table_name_evenement, $data);

        $data['idevenement'] = $this->db->insert_id();

        foreach ($articles as $article) {
            $r_avoir_evenement = new stdClass();
            // r_avoir_evenement

            $r_avoir_evenement->idevenement = $data['idevenement'];
            $r_avoir_evenement->idarticle = $article;

            $result = $this->db->insert('r_avoir_evenement', $r_avoir_evenement);
        }
        return $data;
    }
    public function get_events($debut, $limit)
    {
        return $this->db->select('*')->from('evenement')->limit($limit, $debut)->order_by('idevenement', 'asc')->get()->result();
    }

    public function get_evenement_by_id($idevenement)
    {
        $evenement =  $this->db->select('*')->where('idevenement', $idevenement)->get('evenement')->row();

<<<<<<< HEAD
        $this->db->select('article.*')->from('r_avoir_evenement')->where('r_avoir_evenement.idevenement', $idevenement);
=======
        $this->db->select('article*')->from('r_avoir_evenement')->where('r_avoir_evenement.idevenement', $idevenement);
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
        $this->db->join('article', 'r_avoir_evenement.idarticle=article.idarticle');

        $evenement->produits = $this->db->get()->result();
        return $evenement;
        
    }
    function delete_evenement($idevenement)
    {
        $this->db->where("idevenement", $idevenement);
        $delete = $this->db->delete("evenement");

        return $delete ? true : false;
    }
    public function get_evenements()
    {
        $query = $this->db->select('*')->from("evenement")->get()->result();
        return $query;
    }
    public function update_evenement($data)
    {
        $this->db->where('idevenement', $data['idevenement']);
        $this->db->update($this->table_name_evenement, $data);

        return $this->get_evenement_by_id($data['idevenement']);
    }
    public function get_evenement_fromvendeur($idvendeur)
    {
        return  $this->db->select('*')->from("evenement")->where('idvendeur', $idvendeur)->get()->result();
    }
}
