<!-- Ici on importe les libraries js -->

<script>
  var slideIndex = 0;
  showSlides();

  function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {
      slideIndex = 1
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    setTimeout(showSlides, 8400); // Change image every 2 seconds
  }
</script>
<script src="<?= base_url() . 'assets/jquery/jquery-3.4.1.min.js' ?>"></script>
<!-- <script src="<?= base_url() . 'assets/jquery/custom.js' ?>"></script> -->
<script src="<?= base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
<!-- <script src="<?= base_url() . 'assets/material/js/mdb.min.js' ?>"></script> -->
<!-- <script src="<?= base_url() . 'assets/slick-master/slick-master/slick/slick.js' ?>"></script> -->
<script src="<?= base_url() . 'assets/slick-master/slick-master/slick/slick.min.js' ?>"></script>
<script>
  $(document).ready(function() {
    $('.slider-area').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      prevArrow: '<button class="slide-arrow prev-arrow"></button>',
      nextArrow: '<button class="slide-arrow next-arrow"></button>'
    });
  });
</script>

<!-- <script>
  $(document).ready(function() {
    $('.slider-area').slick({
      infinite: true,
      dots: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow: '<button class="slide-arrow prev-arrow"></brue>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
    });
  });
</script> -->
<script>
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
</script>
</html>