<script>
    (function(t, a, l, k, j, s) {
        s = a.createElement('script');
        s.async = 1;
        s.src = "https://cdn.talkjs.com/talk.js";
        a.head.appendChild(s);
        k = t.Promise;
        t.Talk = {
            v: 1,
            ready: {
                then: function(f) {
                    if (k) return new k(function(r, e) {
                        l.push([f, r, e])
                    });
                    l
                        .push([f])
                },
                catch: function() {
                    return k && new k()
                },
                c: l
            }
        };
    })(window, document, []);
</script>

<!-- container element in which TalkJS will display a chat UI -->
<div id="talkjs-container" style="width: 90%; margin: 30px; height: 500px"><i>Loading chat...</i></div>

<!-- TalkJS initialization code, which we'll customize in the next steps -->
<script>
    Talk.ready.then(function() {

        var me = new Talk.User({
            id: "<?= 'Vendeur_id' . $_SESSION['idvendeur'] ?>",
            name: "<?= $_SESSION['nomvendeur'] ?>",
            email: "<?= $_SESSION['emailvendeur'] ?>",
            photoUrl: "<?= base_url() . 'assets/freetrading.jpg' ?>",
            welcomeMessage: "Hey there! How are you? :-)"
        });


        window.talkSession = new Talk.Session({
            appId: "tfgreQbd",
            me: me
        });


        var conversation = talkSession.getOrCreateConversation(("<?= 'Vendeur_id' . $_SESSION['idvendeur'] ?>"))
        conversation.setParticipant(me);


        var inbox = talkSession.createChatbox(conversation);
        inbox.mount(document.getElementById("talkjs-container"));
    });
</script>