<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- <script src="./bootstrap/js/bootstrap.min.js"></script> -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="./animate.css-master/animate.css">
</head>

<body>
    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div class="bg-info border-right" id="sidebar-wrapper">

                <img src="<?= base_url() . "assets/user.png" ?> " id="newtopdash" alt="">
                <div class="sidebar-heading" style="color: white;text-align:end;"><?php echo  $flow = $_SESSION['nomvendeur']; ?> <span><?php echo  $flow = $_SESSION['telephonevendeur']; ?></span>
                </div>
                <div class="list-group list-group-flush">
                    <a href="<?php echo base_url() . 'Dashboard'  ?>" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">
                        <img src="<?= base_url() . "assets/dashboard.png" ?> " id="news">Dashboard</a>
                    <a href="<?php echo base_url() . 'Pages/produit/' ?>" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">

                        <img src="<?= base_url() . "assets/buy.png" ?> " id="news">Produits</a>
                    <a href="<?php echo base_url() . 'Evenement/evenement_post/' ?> " class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">

                        <img src="<?= base_url() . "assets/calendar.png" ?> " id="news">Evenements</a>
                    <a href="#" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">
                        <img src="<?= base_url() . "assets/improvement.png" ?> " id="news">Chat</a>
                    <a href="#" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">
                        <img src="<?= base_url() . "assets/improvement.png" ?> " id="news">Promotion</a>
                    <a href="#" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">

                        <img src="<?= base_url() . "assets/user (1).png" ?> " id="news">Profile</a>
                    <a href="<?php echo base_url() . 'Vendeur/logout/' ?>" class="list-group-item list-group-item-action list-groupplace bg-info" style="color: white;">
                        <img src="<?= base_url() . "assets/mobile.png" ?> " id="news">Deconnexion</a>
                    <div class="social_media">
                        <li id="socialn"><a href="#"><img src="<?= base_url() . "assets/facebook-3383596_640.png" ?> " id="newtop"> </a></li>

                        <li id="socialn"><a href="#"> <img src="<?= base_url() . "assets/twitter.png" ?> " id="newtop"> </a></li>

                        <li id="socialn"><a href="#"><img src="<?= base_url() . "assets/instagram.jpg" ?> " id="newtop"> </a></li>
                    </div>
                </div>

            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-info" id="menu-toggle"> Menu</button>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav  bg-info ml-auto mt-2 mt-lg-0">
                            <li class="nav-item ">
                                <a class="nav-link" style="color: white;" href="<?php echo base_url() . 'Vendeur/logout/' ?>">LogOut <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" style="color: white;" href="<?php echo base_url()  ?>">Acceuil</a>
                            </li>

                        </ul>
                    </div>
                </nav>

                <div class="contflop">
                    <!-- <h1 class="mt-4">Simple Sidebar</h1> 
            <p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on
              larger screens. When toggled using the button below, the menu will change.</p>
            <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional,
              and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the
              menu when clicked.</p>-->

                    <!-- Card Wider -->
                    <div class="card border-light mb-3">
                        <div class="card-header" style="text-align: center;">
                            <h4>Produits</h4>
                        </div>
                        <div class="card-body">
                            <div class="card border-bg primary mb-3">
                                <div class="card-header bg-transparent border-bg-primary">Produits</div>
                                <div class="card-body text-bg-primary">
                                    <form method="POST" action="<?php echo base_url(); ?>Article/search">
                                        <div class="form-group">
                                            <div class="srcharticle">

                                                <input type="text" class="form-control" id="exampleInputEmail1" name="key" aria-describedby="emailHelp" placeholder="Rechercher un article">
                                                <button type="submit" style="border-bottom-left-radius: inherit;border-top-left-radius: inherit;" class="btn btn-info">Search</button>
                                            </div>
                                            <button type="button" class="btn btn-info newarticles" data-toggle="modal" data-target="#exampleModalLong">New </button>
                                        </div>

                                        <form>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th scope="col">Nom de l'article</th>
                                                        <th scope="col">PrixArticle</th>

                                                        <th scope="col">Libelle_Categorie</th>
                                                        <th scope="col">DescriptionArticle</th>
                                                        <th scope="col" style="padding-left: 1.5em;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($articles  as $roty) : ?>

                                                        <tr>

                                                            <td>
                                                                <?php echo $roty->nomarticle; ?>
                                                            </td>
                                                            <td><?php echo $roty->prixarticle; ?></td>

                                                            <td><?php echo $roty->libelle_categorie; ?></td>
                                                            <td><?php echo $roty->descriptionarticle; ?></td>
                                                            <td style="width: 19%;">

                                                                <img src="<?= base_url() . "assets/edit.png" ?> " data-toggle="modal" data-target="<?= '#exampleModalshort' . $roty->idarticle  ?>" id="editing">

                                                                <a href="<?php echo base_url() . 'Article/article/' . $roty->idarticle ?>">

                                                                    <img src="<?= base_url() . "assets/bin.png" ?> " id="editing" alt="">
                                                                </a>

                                                                <img src="<?= base_url() . "assets/eye (1).png" ?> " id="editing" alt="">

                                                            </td>
                                                        </tr>

                                                        <div class="modal fade" id="<?= 'exampleModalshort' . $roty->idarticle ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg " role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalCenterTitle">Modifier Un Article</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <?php echo form_open_multipart('Article/update_article'); ?>
                                                                    <form method="POST" action="<?php echo base_url(); ?>Article/update_article/ <?php echo $roty->idarticle; ?>">
                                                                        <div class="modal-body">
                                                                            <div class="card border-light mb-3">

                                                                                <div class="card-header">Modifier Un Article</div>
                                                                                <div class="card-body">


                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label>Nom Article</label>
                                                                                            <input type="nomarticle" name="nomarticle" class="form-control" id="inputEmail4" placeholder="Nom Article" value="<?= $roty->nomarticle; ?>">
                                                                                            <input type="text" value="<?= $roty->idarticle; ?>" style="display: none;">
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label>Prixarticle</label>
                                                                                            <input type="" name="prixarticle" class="form-control" id="inputPassword4" placeholder="5000FCFA" value="<?= $roty->prixarticle; ?>">

                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label for="exampleFormControlTextarea1">Description</label>
                                                                                        <textarea class="form-control" name="descriptionarticle" id="exampleFormControlTextarea1" rows="3"><?= $roty->descriptionarticle; ?>
                                                                                        </textarea>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label for="exampleFormControlSelect1">Catégorie</label>
                                                                                        <select class="form-control" name="id_categorie" id="exampleFormControlSelect1">
                                                                                            <?php foreach ($categories as $categorie) :
                                                                                            ?>

                                                                                                <option value='<?= $categorie->id_categorie ?>'><?= $categorie->libelle_categorie ?></option>

                                                                                            <?php endforeach; ?>
                                                                                        </select>
                                                                                    </div>

                                                                                    <br>

                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-5">
                                                                                            <label for="inputCity">Ajouter un lieu</label>
                                                                                            <input value="<?= $roty->localisation; ?>" type="text" name="localisation" class="form-control" id="inputCity">
                                                                                        </div>

                                                                                        <div class="form-group col-md-6" style="margin-left: 3em;">
                                                                                            <label for="exampleFormControlFile1">Choisir Image</label>
                                                                                            <input type="file" name="imagearticle" class="form-control-file" id="exampleFormControlFile1">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <div class="form-check">
                                                                                            <input class="form-check-input" type="checkbox" id="gridCheck">
                                                                                            <label class="form-check-label" for="gridCheck">
                                                                                                Je valide la Modification
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">

                                                                            <button type="submit" name="idarticle" value="<?php echo $roty->idarticle; ?>" class="btn btn-info">Update</button>
                                                                            <button type="button" data-dismiss="modal" class="btn btn-info ">Annuler</button>
                                                                        </div>
                                                                    </form>
                                                                    <?php echo form_close(); ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php endforeach; ?>
                                                </tbody>

                                            </table>
                                        </form>
                                    </form>
                                </div>

                                <div class="card-footer bg-transparent border-bg-primary">Previous</div>

                            </div>


                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Ajouter Un Article</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <?php echo form_open_multipart('Article/article'); ?>
                                        <form method="POST" action="<?php echo base_url(); ?>Article/article">
                                            <div class="modal-body">
                                                <div class="card border-light mb-3">

                                                    <div class="card-header">Ajouter Un Article</div>
                                                    <div class="card-body">


                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label>Nom Article</label>
                                                                <input type="" name="nomarticle" class="form-control" id="inputEmail4" placeholder="Nom Article">

                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Prix</label>
                                                                <input type="" name="prixarticle" class="form-control" id="inputPassword4" placeholder="5000FCFA">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="exampleFormControlTextarea1">Description</label>
                                                            <textarea class="form-control" name="descriptionarticle" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleFormControlSelect1">Catégorie</label>
                                                            <select class="form-control" name="id_categorie" id="exampleFormControlSelect1">
                                                                <?php foreach ($categories as $categorie) :
                                                                ?>

                                                                    <option value='<?= $categorie->id_categorie ?>'><?= $categorie->libelle_categorie ?></option>

                                                                <?php endforeach; ?>
                                                            </select>

                                                        </div>
                                                        <br>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputCity">Ajouter un lieu</label>
                                                                <input type="text" name="localisation" class="form-control" id="inputCity">
                                                            </div>

                                                            <div class="form-group col-md-6" style="margin-left: 3em;">
                                                                <label for="exampleFormControlFile1">Choisir la couverture</label>
                                                                <input type="file" multiple name="imagearticle" class="form-control-file" id="exampleFormControlFile1">

                                                            </div>
                                                            <div class="form-group col-md-6" style="margin-left: 3em;">
                                                                <label for="exampleFormControlFile1">Choisir d'autres images</label>
                                                                <input type="file" multiple name="ressource_article[]" class="form-control-file" id="exampleFormControlFile1">

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                                                <label class="form-check-label" for="gridCheck">
                                                                    Je valide l'ajout
                                                                </label>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info">Ajouter</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-info ">Annuler</button>
                                            </div>
                                        </form>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <!-- Card -->
            </div>
        </div>
    </div>



</body>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

</html>