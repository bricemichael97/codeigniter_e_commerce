<body style="background-color: #f5f5f5; width: 100%;">
    <?php include('menu.php') ?>

    <div id="carouselExampleInterval" style="width: 100%;" class="carousel slide" data-ride="carousel">
        <div class="row carousel-inner slideshow-container">
            <div class="carousel-item active" data-interval="10000">
                <img src="<?= base_url() . "assets/TB1btsIdRBh1e4jSZFhXXcC9VXa-990-400.png" ?> " class="d-block w-100">
            </div>
            <div class="carousel-item" data-interval="2000">
                <img src="<?= base_url() . "assets/TB1x8x9eEY1gK0jSZFMXXaWcVXa-990-400.jpg" ?> " class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="<?= base_url() . "assets/TB1TAdKeuH2gK0jSZFEXXcqMpXa-990-400.jpg" ?> " class="d-block w-100" style="
            ">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- <div class="row slideshow-container" style="width: 100%;">

        <div class="col-9 mySlides fade_perso">

            <img src="<?= base_url() . "assets/TB1btsIdRBh1e4jSZFhXXcC9VXa-990-400.png" ?> " id="banner" style="height: 67vh;">


        </div>

        <div class=" col-9 mySlides fade_perso">

            <img src="<?= base_url() . "assets/TB1x8x9eEY1gK0jSZFMXXaWcVXa-990-400.jpg" ?> " id="banner" style="height: 67vh;">

            <div class="carousel-caption">

            </div>
        </div>

        <div class="col-9 mySlides fade_perso">

            <img src="<?= base_url() . "assets/TB1TAdKeuH2gK0jSZFEXXcqMpXa-990-400.jpg" ?> " id="banner" style="
            height: 67vh;">

        </div>

    </div>
    <br>

    <div style="text-align:center">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
    </div> -->
    <div class="separator">


    </div>

    <div class=" d-flex grpcaterogie" style="margin-top: 2em;width:100%">
        <div class="">
            <ul class="list-group list-grp">

                <li class="list-group-item  bg-info d-flex justify-content-between align-items-center">
                    <a class="categorie" style="color: white;" href="">TOUS LES CATEGORIES</a>
                    <span class="badge badge-primary badge-pill"></span>
                </li>
                <?php foreach ($categories as $even) : ?>
                    <li class="list-group-item list-categorie d-flex justify-content-between align-items-center" style="height: 10.4vh;">
                        <a class="categorie" href="<?php echo base_url() . 'Categorie/cat/' . $even->id_categorie ?>"><?php echo $even->libelle_categorie; ?></a>
                        <span style="color: white;" class="badge bg-info badge-primary badge-pill"><?php echo $even->nombrearticle; ?> </span>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!-- <div class="card">
            <div class="card-header">
                Featured
            </div>
            <div class="card-body">
                <h5 class="card-title">Special title treatment</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div> -->
        <div class="card recentproduits">
            <div class="card-header confi">
                <div>PRODUITS RECENTS</div>

                <div style="    margin-right: -27px;display: inline-flex;">

                </div>
            </div>

            <div class="slider-area card-body min">

                <?php foreach ($articles_slick as $sub_art) : ?>
                    <div id="slick_sop">
                        <?php foreach ($sub_art as $art) : ?>


                            <div class="create">
                                <div class="newitem">
                                    <p id="nouveaute">NEW</p>
                                </div>
                                <li class="card store horse" style="margin-top: 1.2em;width:18em">
                                    <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/article/' . $art->idarticle ?>">
                                        <img src=" <?php echo $art->imagearticle; ?> " id="first_image" class="card-img-top" alt="...">
                                        <div class="card-text likes">
                                            <h3>13 jaime</h3>
                                            <h5>15 commentaire</h5>
                                            <h6>0 signalement</h6>
                                        </div>


                                        <div class="card-body cardy">


                                            <ul class="somme">

                                                <li class="card-text article"> <?php echo $art->nomarticle; ?></li>
                                                <li class="card-text price"><?php echo $art->prixarticle; ?> FCFA</li>
                                                <li class="card-text article_date"> <?php echo $art->date_ajout; ?></li>
                                                <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $art->nomvendeur; ?></li>
                                            </ul>

                                        </div>
                                    </a>
                                </li>

                            </div>

                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>

            </div>

        </div>
    </div>
    <br>
    <hr style="    margin-left: 6em; margin-right: 6em;">
    <br>
    <div class="dividetopcat">
        <div class=" card " style="width:100%">
            <div class="card-header conficatre">
                <div class="c">
                    <h5 class="recent2">CATEGORIES RECOMMANDES

                    </h5>
                </div>
                <div class="elect">

                    <i class="fas fa-mobile" style="color: white;"></i>
                    <a style="    color: white;" href="#">
                        Télephone et Tablettes
                    </a>
                    <div class="lineup">
                    </div>
                </div>

                <div class="elect">
                    <i class="fas fa-car" style="color: white;"></i>
                    <a style="    color: white;" href="#">
                        Véhicules
                    </a>
                    <div class="lineup">

                    </div>
                </div>
                <div class="elect">
                    <i class="fas fa-home" style="color: white;"></i>
                    <a style="    color: white;" href="#">
                        Immobilier
                    </a>
                    <div class="lineup">

                    </div>
                </div>
                <div class="arroe">
                    <a href=""> <img src="<?= base_url() . "assets/left arrow.png" ?> " id="chevron_left" alt=""></a>
                    <a href=""> <img src="<?= base_url() . "assets/right arrow.png" ?> " id="chevron_right" alt=""></a>

                </div>
            </div>


            <div class=" card-body minhot">


                <?php foreach ($articles as $art) : ?>
                    <div class="createcat" style="margin-bottom: 2em;
    margin-top: 2em;">
                        <div class="newitemcat">
                            <p id="nouveautecat">HOT</p>

                        </div>
                        <li class="card storecat horse1">
                            <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/article/' . $art->idarticle ?>">
                                <img src="<?php echo $art->imagearticle; ?> " style="width: 100%;height: 20vh; object-fit: fill;" class="card-img-top" alt="...">
                                <div class="card-text likes2">
                                    <h3>13 jaime</h3>
                                    <h5>15 commentaire</h5>
                                    <h6>0 signalement</h6>
                                </div>

                                <div class="card-body cardy">
                                    <ul class="somme">
                                        <li class="card-text article"> <?php echo $art->nomarticle; ?></li>
                                        <li class="card-text price"><?php echo $art->prixarticle; ?> FCFA</li>
                                        <li class="card-text article_date"> <?php echo $art->date_ajout; ?></li>
                                        <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $art->nomvendeur; ?></li>
                                    </ul>

                                </div>
                            </a>
                        </li>
                    </div>

                <?php endforeach; ?>

            </div>

            <div class="separator">


            </div>
        </div>
    </div>
    <br>
    <hr style="    margin-left: 6em; margin-right: 6em;">
    <br>
    <div class="dividetopcat">
        <div class=" card mainevent" style="width:100%">
            <div class="card-header confi">
                <div class="evenemnt">
                    <h5>EVENEMENT </h5>
                </div>
                <div style="    margin-right: -41px;">
                    <a href=""> <img src="<?= base_url() . "assets/left arrow.png" ?> " id="chevron_left" alt=""> </a>
                    <a href=""> <img src="<?= base_url() . "assets/right arrow.png" ?> " id="chevron_right" alt=""></a>
                </div>

            </div>

            <div class="card-body minhot">
                <?php foreach ($evenements as $arts) : ?>
                    <div class="card cardevenement" style="width: 18rem;">
                        <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/evenements/' . $arts->idevenement ?>">
                            <div class="inner">

                                <img src="<?php echo $arts->imageevenement; ?> " id="first_imageevent" class="card-img-top" alt="...">
                            </div>

                            <div class="card-body bodyevent">
                                <h5 class="card-title" style="color:  #17a2b8!important;margin-top: -11px;">Description</h5>
                                <p class="card-text article_dates "><?php echo $arts->descriptionevenement; ?></p>
                                <p class="card-text price" style="font-weight: 700;">Evenement portant sur :</p>
                                <p class="article_date" > <?php echo $arts->nomevenement; ?></p>
                            </div>

                        </a>
                    </div>


                <?php endforeach; ?>
            </div>
            <!-- Vu récemment -->


        </div>
    </div>
    <br>
    <hr style="    margin-left: 6em; margin-right: 6em;">
    <br>
    <div class="dividetopcat">
        <div class=" card mainevent" style="width:100%">
            <div class="card-header confi">
                <div class="evenemnt">
                    <h5>Vu récemment </h5>
                </div>
                <div style="    margin-right: -41px;">
                    <a href=""> <img src="<?= base_url() . "assets/left arrow.png" ?> " id="chevronxx" alt=""> </a>
                    <a href=""> <img src="<?= base_url() . "assets/right arrow.png" ?> " id="chevronx1" alt=""></a>
                </div>

            </div>

            <div class="card-body minhot">
                <?php foreach ($articles as $art) : ?>
                    <div class="createcat" style="margin-bottom: 2em;margin-top: 2em;">
                        <div class="newitemcat">
                            <p id="nouveautecat">HOT</p>

                        </div>
                        <li class="card storecat horse1"> 
                            <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/article/' . $art->idarticle ?>">
                                <img src="<?php echo $art->imagearticle; ?> " style="width: 100%;height: 20vh; object-fit: fill;" class="card-img-top" alt="...">
                                <div class="card-text likes2">
                                    <h3>13 j'aime</h3>
                                    <h5>15 commentaires</h5>
                                    <h6>0 signalement</h6>
                                </div>
                            </a>
                            <div class="card-body cardy">
                                <ul class="somme">
                                    <li class="card-text article"> <?php echo $art->nomarticle; ?></li>
                                    <li class="card-text price"><?php echo $art->prixarticle; ?> FCFA</li>
                                    <li class="card-text article_date"> <?php echo $art->date_ajout; ?></li>
                                    <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $art->nomvendeur; ?></li>
                                </ul>

                            </div>

                        </li>
                    </div>

                <?php endforeach; ?>
                <!-- Vu récemment -->
            </div>
        </div>

    </div>
    <?php include('footer_page.php') ?>

</body>


<!-- <div class="header " style="background: cadetblue;"> --
        <div class="top_bar_header" style="height: 30px;">
            <div class="container-fluid ">
                <div class="row justify-content-between ">

                    <div class="col-8   text-white ">
                        <div class="row justify-content-between">
                            <div class="col-3 ">

                                <div class="app_lang">

                                    Francais

                                </div>
                            </div>
                            <div class="col">

                                <div class="contact_seed">

                                    bricemichael808@gmail.com

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4    text-white ">
                        <div class="row justify-content-around">
                            <div class="col-5">

                                <div class="app_lang">

                                    Protection du Client

                                </div>
                            </div>
                            <div class="col">

                                <div class="contact_seed">

                                    Compte

                                </div>
                            </div>
                            <div class="col-4">

                                <div class="contact_seed">
                                    Aide

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        #cbcbcb
    </div>*/
     <div class="row slideshow-container">

        <div class="col-10 mySlides fade_perso">

            <img src="<?= base_url() . "assets/interior-4400306_640.jpg" ?> " id="banner" style="width: 126%;height: 67vh;">

            <div class="text" style="text-align: center;">
                <h3 class="animated zoomIn" style="animation-delay: 1s;">
                    BUY YOUR SHOE
                </h3>
                <h5 class="animated zoomIn" style="animation-delay: 3s;">VERY EASY SHOPPING</h5>
                <h6 class="animated zoomIn" style="animation-delay: 5s;"> <button type="button" class="btn btn-info">
                        <a id="shopnow" href="#">SHOP NOW</a></button> </h6>
            </div>
        </div>

        <div class=" col-10 mySlides fade_perso">

            <img src="<?= base_url() . "assets/spaces-1821646_640.jpg" ?> " id="banner" style="
                width: 126%;
                height: 67vh;">
            <div class="carousel-caption">
                <div class="text" style="text-align: center;">
                    <h3 class=" zoom" style="animation-delay: 1s;">
                        BUY YOUR SUFFER
                    </h3>
                    <h5 class=" zoom" style="animation-delay: 3s;">VERY EASY SHOPPING</h5>
                    <h6 class=" zoom" style="animation-delay: 5s;"> <button type="button" class="btn btn-info"> <a id="shopnow" href="#">SHOP NOW</a></button> </h6>
                </div>
            </div>
        </div>

        <div class="col-10 mySlides fade_perso">

            <img src="<?= base_url() . "assets/interior-4400306_640.jpg" ?> " id="banner" style="width: 126%;
            height: 67vh;">
            <div class="text" style="text-align: center;">
                <h3 class="zoom1" style="animation-delay: 1s;">
                    BUY YOUR SUFFER
                </h3>
                <h5 class=" zoom1" style="animation-delay: 3s;">VERY EASY SHOPPING</h5>
                <h6 class=" zoom1" style="animation-delay: 5s;"> <button type="button" class="btn btn-info"> <a id="shopnow" href="#">SHOP NOW</a></button> </h6>
            </div>
        </div>

    </div>
    <br>

    <div style="text-align:center">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
    <div class="Adresse">
            <div class="findus">
                <div class="company">
                    <div class="adresseimg">
                        <img src="<?= base_url() . "assets/navigation-5109674_640.png" ?> " id="navigate" alt="">
                    </div>
                    <div class="find1">
                        <h5>Trouve Nous </h5>
                        <h6>NOTRE ADRESSE</h6>
                    </div>
                </div>

            </div>
            <br>
            <div class="confiden">

                <img src="<?= base_url() . "assets/map-42871_640.png" ?> " id="streadresse" alt="">
                <p style="    margin-left: 2.2em;
                margin-top: -6px;">15, My Company, ACACIA MONTEE ROND POINT EXPRESS, YAOUNDÉ</p>

            </div>
            <div class="confiden">
                <img src="<?= base_url() . "assets/phone-651704_640.png" ?> " id="streadresse1" alt="">
                <p style="    margin-left: 2.2em;
                margin-top: -6px;">+237695563032</p>

            </div>
            <div class="confiden">

                <img src="<?= base_url() . "assets/email-309678_640.png" ?> " id="streadresse" alt="">
                <p style="    margin-left: 2.2em;
                margin-top: -6px;">bricemichael808@gmail.com</p>

            </div>
        </div>

        <div class="Adresse">
            <div class="findus">
                <div class="company">
                    <div class="adresseimg">
                        <img src="<?= base_url() . "assets/paper-planes-3128885_640.png" ?> " id="navigate" alt="">
                    </div>
                    <div class="find4">
                        <h5>Mise à jour quotidienne </h5>
                        <h6 style="    padding-top: 3em;
                        margin-left: -4em;">
                            Inscrivez-vous à la Newsletter</h6>
                    </div>
                </div>

            </div>
            <br>
            <div class="search-email">

                <input type="text" class="form-sendmail" placeholder="Entrez votre adresse e-mail" name="" id="">
                <span class="input-group-email"><i class="fa fa-paper-plane plane-fav" aria-hidden="true"></i>
            </div>
        </div>
        <div class="Adresse">
            <div class="findus">
                <div class="company">
                    <div class="adresseimg">

                        <img src="<?= base_url() . "assets/price-2389232_640.png" ?> " class="rounded-circle" id="navigatenes" alt="">
                    </div>
                    <div class="find2">
                        <h5>
                            Meilleures offres</h5>
                        <h6 style="    padding-top: 1em;
                       ">PROMOTION</h6>
                    </div>
                </div>

            </div>
            <br>
            <div class="company2">
                <div class="adresseimg">

                    <img src="<?= base_url() . "assets/discount-2542621_640.png" ?> " id="navigate2" alt="">
                </div>
                <div class="find2">
                    <h5>
                        Meilleures offres</h5>
                    <h6>PROMOTION</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flew-row" style="width: 100%; justify-content: space-evenly;padding-left: 1em;padding-right: 1em;height: auto;margin-top:2em">
        <div class="jumbotron jumbotron-fluid" style="width: 20%;    background: white;">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-2">
                        <img src="<?= base_url() . "assets/navigation-5109674_640.png" ?> " id="navigate" alt="">
                    </div>
                    <div class="col">
                        <h5>Trouve Nous </h5>
                        <h6>NOTRE ADRESSE</h6>
                    </div>


                </div>
                <div class="d-flex flex-column">
                    <div class="confiden">
                        <img src="<?= base_url() . "assets/map-42871_640.png" ?> " id="streadmail" alt=""> 15, My Company, ACACIA MONTEE ROND POINT EXPRESS, YAOUNDÉ

                    </div>
                    <div class="confiden">
                        <img src="<?= base_url() . "assets/phone-651704_640.png" ?> " id="streadmail" alt=""> +237695563032

                    </div>
                    <div class="confiden">

                        <img src="<?= base_url() . "assets/email-309678_640.png" ?> " id="streadmail" alt=""> bricemichael808@gmail.com

                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid" style="width: 20%;    background: white;">
            <div class="container">
                <div class="row justify-content-center" style="margin-top: 2em;">

                    <div class="col-2">
                        <img src="<?= base_url() . "assets/paper-planes-3128885_640.png" ?> " id="navigate" alt="">
                    </div>
                    <div class="col">
                        <h5>Mise à jour quotidienne </h5>

                    </div>
                </div>
                <br>
                <h6 style="text-align: center;">
                    Inscrivez-vous à la Newsletter</h6>

                <div class="search-news">

                    <input type="text" class="form-sendmaile" placeholder="Entrez votre adresse e-mail" name="" id="">
                    <span class="input-group-email " style="width: 13%;" id="emails-input-group"><i class="fa fa-paper-plane plane-favecon" aria-hidden="true"></i>
                </div>

            </div>
        </div>
        <div class="jumbotron jumbotron-fluid" style="width: 20%;    background: white;">
            <div class="container" style="margin-top: 2em;text-align:center">
                <div class="row justify-content-center">

                    <div class="col-2">
                        <img src="<?= base_url() . "assets/price.png" ?> " id="navigateoffres" alt="">
                    </div>
                    <div class="col">
                        <h5>
                            Meilleures offres </h5>
                        <h6>PROMOTION</h6>
                    </div>


                </div>
                <div class="row justify-content-center" style="margin-top: 3em;">

                    <div class="col-2">
                        <img src="<?= base_url() . "assets/discount.png" ?> " id="navigateoffres" alt="">
                    </div>
                    <div class="col">
                        <h5>
                            Meilleures offres </h5>
                        <h6>PROMOTION</h6>
                    </div>


                </div>
            </div>
        </div>
    </div>