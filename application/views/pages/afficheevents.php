<body>

    <body style="background-color: #f5f5f5;">
        <?php include('menu.php') ?>

        <div class="row" style="margin-left: 5em;margin-right: 5em;">
            <div class="col-lg-12">
                <img src="<?= $evenement->imageevenement ?>" alt="" style="width: 100%;height: 60vh;object-fit: cover;">
                <h1 style="text-align: center;"><?= $evenement->nomevenement ?></h1>
            </div>

        </div>
        <div class="card" style="margin-left: 5em;margin-right: 5em;">
            <div class="card-header" style="background: #17a2b8!important;text-align:center;color:white">
                Articles
            </div>
            <div class="card-body ">
                <div class="conatainer" style="display: flex; flex-wrap: wrap; padding-left: 4em; padding-right: 4em; justify-content: space-around;">
                    <?php for ($i = 0; $i < count($evenement->produits); $i++) : ?>
                        <div class="card minhot " style="width: 15rem;margin-bottom: 2em;list-style: none;">
                            <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/article/' . $evenement->produits[$i]->idarticle ?>">
                                <div class="inner">
                                    <img class="d-block " id="cat_show_articles" style="height: 22vh;object-fit: contain;object-position: center center;width:100%" class="card-img-top" src=" <?php echo $evenement->produits[$i]->imagearticle; ?> " alt="First slide">
                                </div> 
                                <div class="card-body ">

                                    <li class="card-text article"> <?php echo $evenement->produits[$i]->nomarticle; ?></li>
                                    <li class="card-text price"><?php echo $evenement->produits[$i]->prixarticle; ?> FCFA</li>
                                    <li class="card-text article_date"> <?php echo $evenement->produits[$i]->date_ajout; ?></li>
                                    <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $evenement->produits[$i]->nomvendeur; ?></li>

                                </div>
                            </a>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>

        <?php include('footer_page.php') ?>

    </body>
    <!-- <?php for ($i = 0; $i < count($evenement->produits); $i++) : ?>
        <li class="card storecat ">
            <a class="nav-link">
                <img class="d-block " style=" max-height: 43vh;width: 100%;object-fit: contain;" src=" <?php echo $evenement->produits[$i]->imagearticle; ?> " alt="First slide">

                <div class="card-body cardy">
                    <ul class="somme">
                        <li class="card-text article"> <?php echo $evenement->produits[$i]->nomarticle; ?></li>
                        <li class="card-text price"><?php echo $evenement->produits[$i]->prixarticle; ?> FCFA</li>
                        <li class="card-text article"> <?php echo $evenement->produits[$i]->date_ajout; ?></li>
                        <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $evenement->produits[$i]->nomvendeur; ?></li>
                    </ul>

                </div>
            </a>
        </li>
    <?php endfor; ?> -->