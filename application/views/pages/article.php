<body style="background-color: #f5f5f5;">
    <?php include('menu.php') ?>
    <div class="efferstate">
        <div class="row skype">
            <div class="col-sm-6 rowtype ">
                <div class="col-sm-12" style="height: 110vh;">
                    <!--Carousel Wrapper-->
                    <div class="container" style="width: 100%;">
                        <div id="carousel-thumb" style="width: 100%;" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner" style="width: 100%;" role="listbox">

                                <div class="carousel-item active">
                                    <img style=" max-height: 43vh;width: 100%;object-fit: contain;" class="d-block w-100" src=" <?php echo $article->imagearticle; ?> " alt="First slide">
                                </div>
                                <?php for ($i = 0; $i < count($resource_articles); $i++) : ?>
                                    <div class="carousel-item ">
                                        <img class="d-block " style=" max-height: 43vh;width: 100%;object-fit: contain;" src=" <?php echo $resource_articles[$i]->lien_image; ?> " alt="First slide">
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->


                            <a class="carousel-control-prev" style="    background: dimgray;
                            width: 7%;" href="#carousel-thumb" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" style="    background: dimgray;
                            width: 7%;" href="#carousel-thumb" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-thumb" data-slide-to="0" class="active">
                                </li>
                                <?php for ($i = 1; $i < count($resource_articles); $i++) : ?>
                                    <li data-target="#carousel-thumb" data-slide-to=<?php echo $i; ?> class="">

                                    </li>

                                <?php endfor; ?>
                            </ol>
                            <ol class="carousel-indicators" style="margin-top: -5em;
                            bottom: -213px;">
                                <img data-target="#carousel-thumb" data-slide-to="0" src=" <?php echo $article->imagearticle; ?> " class="active" width="100">
                                <?php for ($i = 0; $i < count($resource_articles); $i++) : ?>
                                    <img data-target="#carousel-thumb" data-slide-to=<?php echo $i; ?> src=" <?php echo $resource_articles[$i]->lien_image; ?> " width="100">

                                <?php endfor; ?>
                            </ol>
                        </div>

                    </div>

                </div>


                <div class="detailproduit pl-2 pr-2">
                    <ul class="notecomjaime">
                        <li class="notejaime">
                            <?= $article->nombrenote ?> <i class="far fa-thumbs-up"></i>
                            <a href="<?= base_url() . 'Article/like/' . $idarticle ?>" id="ignaler">Jaime </a>
                        </li>

                        <li class="notejaime">
                            <?= count($commentaires) ?> <i class="far fa-comment-dots"></i>
                            <span id="ignaler">Commentaires </span>
                        </li>
                    </ul>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($_SESSION['idvendeur'])) : ?>
                                <div class="mw-100 overflow--hidden">
                                    <h4>Laissez un avis sur ce produit</h4>
                                    <form action="<?= base_url() . 'Article/add_commenaire' ?>" method="POST">
                                        <div class="form-group">
                                            <label for="Comment">Votre Commentaire</label>
                                            <textarea class="form-control" id="Comment" rows="6" name="commentaire"></textarea>
                                            <input type="text" name="idvendeur" style="display: none;" value="<?= $_SESSION['idvendeur'] ?>">
                                            <input type="text" name="nomvendeur" style="display: none;" value="<?= $_SESSION['nomvendeur'] ?>">
                                            <input type="text" name="id_article" style="display: none;" value="<?= $idarticle ?>">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 mb-1">
                                                <button class="btn btn-primary right">Envoyer</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php else : ?>
                                <div class="mw-100 overflow--hidden">
                                    <!-- <h4>Laissez un avis sur ce produit</h4> -->
                                    <button class="btn" data-toggle="modal" data-target="#modalLRForm">Veuillez vos connecter pour commenter</button>
                                </div>
                            <?php endif; ?>
                            <div class="mw-100 overflow--hidden ">
                                <?php foreach ($commentaires as $commentaire) : ?>

                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-lg-1 d-flex justify-content-center flex-column align-content-center align-items-center">
<<<<<<< HEAD
                                            <img style="height: 32px;width:32px" src="<?= base_url() . 'assets/user2.png' ?>" class="image-responsive rounded-circle" alt="">
                                            <span><?= $commentaire->nomvendeur ?> </span>
=======
                                            <img style="height: 32px;width:32px" src="<?= base_url() . 'assets/freetrading.jpg' ?>" class="image-responsive rounded-circle" alt="">
                                            <span>User </span>
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
                                        </div>
                                        <div class="col-sm-12 col-lg-11">
                                            <div class="card">
                                                <div class="card-body">
                                                    <?= $commentaire->commentaire ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!--/.Carousel Wrapper-->



                <!--/.Slides-->
                <!--Controls-->


                <div class="autresinter">
                    <h3>
                        Autres articles qui peuvent vous intéresser
                    </h3>
                    <div class="row" style="margin-left: .1em;">

                        <?php foreach ($articles as $art) : ?>
                            <div class="col-lg-3">
                                <div class="createcat" style="margin-bottom: 2em;margin-top: 2em;">

                                    <li class="card storecat horse1">
                                        <a class="nav-link" style="padding: 0;" href="<?php echo base_url() . 'Pages/article/' . $art->idarticle ?>">
                                            <img style="height: 150px;" src="<?php echo $art->imagearticle; ?> " class="card-img-top" alt="...">
                                            <div class="card-text likes2">
                                                <h3>13 jaime</h3>
                                                <h5>15 commentaire</h5>
                                                <h6>0 signalement</h6>
                                            </div>
                                        </a>
                                        <div class="card-body cardy">
                                            <ul class="somme">
                                                <li class="card-text article"> <?php echo $art->nomarticle; ?></li>
                                                <li class="card-text price"><?php echo $art->prixarticle; ?> FCFA</li>
                                                <li class="card-text article"> <?php echo $art->date_ajout; ?></li>
                                                <li class="card-text" style="font-size: 16px;font-weight: 300;color: gray;">Vendu Par : <?php echo $art->nomvendeur; ?></li>
                                            </ul>

                                        </div>

                                    </li>
                                </div>
                            </div>



                        <?php endforeach; ?>

                    </div>

                </div>
            </div>




            <div class="col-sm-4 ">
                <ul class="list-group" style="margin-top: 3em;">
                    <li class="list-group-item bg-info" style="color: white;">A propos</li>
                    <li class="list-group-item">
                        <h1><?php echo $art->nomarticle; ?></h1>
                    </li>

                    <li class="list-group-item">
                        <?php echo $article->descriptionarticle; ?>

                    </li>
                    <li class="list-group-item">
                        <ul class="catconsec">
                            <li class="catdates">
                                Catégorie : <a href="<?php echo base_url() . 'Categorie/cat/' . $art->id_categorie ?>"><?= $article->libelle_categorie ?></a>
                            </li>
                            <li class="catdates">
<<<<<<< HEAD
                                Date et Heure : <?php echo $art->date_ajout; ?>
=======
                                Date et Heure : <?= date('Y-m-d', (int) $art->date_ajout) ?>
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
                            </li>
                        </ul>
                    </li>
                    <li class="list-group-item d-flex" style="    position: relative;">
                        <div>
<<<<<<< HEAD
                            <a href="<?php echo base_url() . 'Pages/signaler/' ?>">
=======
                            <a href="">
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
                                <img src="<?= base_url() . "assets/info.png" ?>" class="signlerrouge" alt="">
                                <span id="ignal">Signaler l'annonce </span>
                            </a>

                        </div>
                        <div>
                            <a href="">
                                <img src="<?= base_url() . "assets/heart.png" ?>" class="signlerrouge" alt="">
                                <span id="ignali">Ajouter aux favoris </span>
                            </a>

                        </div>
                    </li>
                    <!-- <li class="list-group-item" style="text-align: center;">
                    </li> -->


                    </li>
                </ul>
                <div class="detailproduit">
                    <img src="<?= base_url() . "assets/user2.png" ?>" class="user2" alt="">
                    <p><?php echo $art->nomvendeur; ?></p>
                    <p style="color: #ccc;"><?= $vendeur->nombre_darticless ?> annonce en ligne</p>
                    <hr>
                    <div>
                        <a href="<?= base_url() . 'Pages/chatPage/' . $vendeur->idvendeur ?>" type="button" style="width: 94%;color: white;" class="btn btn-info">
                            <img src="<?= base_url() . "assets/chat (4).png" ?>" class="unnamed" alt="">Envoyer un message
                        </a>
                    </div>
                    <hr>
                    <p>OU</p>
                    <hr>

                    <a type="button" href="<?= 'tel:+237' . $vendeur->telephonevendeur ?>" style="width: 48%; padding-left: 2em;color: white;" class="btn btn-info">
                        <img src="<?= base_url() . "assets/phone (1).png" ?>" class="signlerrougers" alt=""> Voir le numéro
                    </a>

                    <a type="button" href="<?= 'mailto:' . $vendeur->emailvendeur ?>" style="width: 44%;padding-left: 2em;color: white;" class="btn btn-info">
                        <img src="<?= base_url() . "assets/mail.png" ?>" class="signlerrougers" alt="">Adresse email</a>


                    <br>
                    <div>
                        <a type="button" style="width: 94%;margin-top: 1em;margin-bottom: 1.5em;   
                        padding-left: 2em;" class="btn btn-success" href="<?= 'https://api.whatsapp.com/send?phone=+237' . $vendeur->telephonevendeur . '&text=Bonjour Monsieur, je suis interessé par votre produit ' . $article->nomarticle . ' disponible sur ' . $share_link ?>">
                            <img src="<?= base_url() . 'assets/whatsapp (3).png' ?>" class="unnamed" alt="">Whatsapp
                        </a>

                    </div>
                </div>

                <div class="row no-gutters mt-2 mb-2 align-items-center">
                    <div class="col-3">
                        <div class="product-description-label mt-0">Partager:</div>
                    </div>
                    <div class="col-9">
                        <div id="share" class="d-flex justify-content-around align-items-center">
                            <div style="background-color: #1092F3;height: 40px;width: 40px;border-radius: 10px;">
                                <a class="text-white w-100 h-100 d-flex justify-content-center align-items-center" title="Share this page on facebook" href="<?= "http://www.facebook.com/share.php?u=" . $share_link ?>" target="_blank">
                                    <i class="fa fa-facebook-f"></i>
                                </a>
                            </div>
                            <div style="background-color: #006599;height: 40px;width: 40px;border-radius: 10px;">
                                <a class="text-white w-100 h-100 d-flex justify-content-center align-items-center" title="Share this page on linkedin" href="<?= 'http://www.linkedin.com/shareArticle?mini=true&amp;url=' . $share_link . '&amp;title=' . $article->nomarticle . '&amp;summary=' . $article->nomarticle . '&amp;source=' . base_url() ?>">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>
                            <div style="background-color: #1DA1F2;height: 40px;width: 40px;border-radius: 10px;">
<<<<<<< HEAD
                                <a class="text-white w-100 h-100 d-flex justify-content-center align-items-center" title="Share this page on twitter" href="<?= "https://www.twitter.com/share?url=" . $share_link ?>" target="_blank">
=======
                                <a class="text-white w-100 h-100 d-flex justify-content-center align-items-center" title="Share this page on twitter" href="<?= "https:www//twitter.com/share?url=" . $share_link ?>" target="_blank">
>>>>>>> 25c44d07f64bef604d0e22910af8ec6cb97ef367
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </div>
                            <div style="background-color: #26A4E3;height: 40px;width: 40px;border-radius: 10px;">
                                <a class="text-white w-100 h-100 d-flex justify-content-center align-items-center" title="Share this page on telegram" href="<?= "https://t.me/share/url?url=" . $share_link . "&text=" . $article->descriptionarticle ?>" target="_blank">
                                    <i class="fa fa-telegram"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- <ul class="list-group partagetwit">
                    <li class="list-group-item bg-info" style="color: white;">Localisation</li>

                    <li class="list-group-item">
                        <p style="margin-left: 2em;"> <img src="./navigation-5109674_640.png" class="unnamed" alt="">
                            Douala, Cameroon </p>
                        <img src="<?= base_url() . "assets/location1.JPG" ?>" id="locat1" alt="">

                    </li>
                </ul> -->

            </div>


        </div>
    </div>

    <?php include('footer_page.php') ?>

</body>