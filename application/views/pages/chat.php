<?php if (isset($_SESSION['type_user'])) : ?>

    <script>
        (function(t, a, l, k, j, s) {
            s = a.createElement('script');
            s.async = 1;
            s.src = "https://cdn.talkjs.com/talk.js";
            a.head.appendChild(s);
            k = t.Promise;
            t.Talk = {
                v: 1,
                ready: {
                    then: function(f) {
                        if (k) return new k(function(r, e) {
                            l.push([f, r, e])
                        });
                        l
                            .push([f])
                    },
                    catch: function() {
                        return k && new k()
                    },
                    c: l
                }
            };
        })(window, document, []);
    </script>

    <!-- container element in which TalkJS will display a chat UI -->
    <div id="talkjs-container" style="width: 90%; margin: 30px; height: 500px"><i>Loading chat...</i></div>

    <!-- TalkJS initialization code, which we'll customize in the next steps -->
    <script>
        Talk.ready.then(function() {

            var me = new Talk.User({
                id: "<?= 'Vendeur_id' . $_SESSION['idvendeur'] ?>",
                name: "<?= $_SESSION['nomvendeur'] ?>",
                email: "<?= $_SESSION['emailvendeur'] ?>",
                photoUrl: "<?= base_url() . 'assets/freetrading.jpg' ?>",
                welcomeMessage: "Hey there! How are you? :-)"
            });

            var other = new Talk.User({
                id: "<?= 'Vendeur_id' . $idvendeur ?>",
                name: "<?= $vendeur->nomvendeur ?>",
                email: "<?= $vendeur->emailvendeur ?>",
                photoUrl: "<?= base_url() . 'assets/freetrading.jpg' ?>",
                welcomeMessage: "Hey there! How are you? :-)"
            });

            window.talkSession = new Talk.Session({
                appId: "tfgreQbd",
                me: me
            });


            var conversation = talkSession.getOrCreateConversation(("<?= 'Vendeur_id' . $_SESSION['idvendeur'] . '_' . $idvendeur ?>"))
            conversation.setParticipant(me);
            conversation.setParticipant(other);


            var inbox = talkSession.createChatbox(conversation);
            inbox.mount(document.getElementById("talkjs-container"));
        });
    </script>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <ul class="nav nav-tabs md-tabs tabs-2 bg-info darken-3" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                                Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                                Register</a>
                        </li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 7-->
                        <div class="tab-pane fade in active show " style="background-color: white;" id="panel7" role="tabpanel">
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert danger">
                                    <?php $this->session->flashdata('message') ?>
                                </div>
                            <?php } ?>

                            <form method="POST" action="<?php echo base_url(); ?>Vendeur/login">
                                <!--Body-->
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <i class="fas fa-envelope prefix"></i> <label data-error="wrong" data-success="right" for="modalLRInput10">Your
                                            email</label>
                                        <input type="" name="emailvendeur" id="modalLRInput10" class="form-control form-control-sm validate">

                                    </div>

                                    <div class="md-form form-sm mb-4">
                                        <i class="fas fa-lock prefix"></i> <label data-error="wrong" data-success="right" for="modalLRInput11">Your
                                            password</label>
                                        <input type="password" name="passwordvendeur" id="modalLRInput11" class="form-control form-control-sm validate">

                                    </div>
                                    <div class="text-center mt-2">
                                        <button class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
                                    </div>
                                </div>
                                <!--Footer-->
                                <div class="modal-footer">
                                    <!-- <div class="options text-center text-md-right mt-1">
                <p>Not a member? <a href="#" style="color: #17a2b8;" class="blue-text">Sign Up</a></p>
                <p>Forgot <a href="#" style="color: #17a2b8;" class="blue-text">Password?</a></p>
            </div> -->
                                    <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                        <!--/.Panel 7-->

                        <!--Panel 8-->
                        <div class="tab-pane fade" style="background-color: white;" id="panel8" role="tabpanel">

                            <!--Body-->
                            <div class="card-body px-lg-5 pt-0">

                                <!-- Form -->
                                <form class="text-center" style="color: #757575;" method="POST" action="<?php echo base_url(); ?>Vendeur/vendeur">
                                    <br>

                                    <div class="form-row">
                                        <div class="col">
                                            <!-- First name -->
                                            <div class="md-form">
                                                <label for="materialRegisterFormFirstName" style="float: left;" id="labelstrong">Nom</label>
                                                <input required type="text" name="nomvendeur" id="materialRegisterFormFirstName" class="form-control">

                                            </div>
                                        </div>
                                        <br>

                                        <div class="col">
                                            <!-- Last name -->
                                            <div class="md-form">
                                                <label for="materialRegisterFormLastName" style="float: left;">Prenom</label>
                                                <input type="text" name="prenomvendeur" id="materialRegisterFormLastName" class="form-control">

                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <!-- E-mail -->
                                    <div class="md-form mt-0">
                                        <label for="materialRegisterFormEmail" style="float: left;" id="labelstrong">E-mail</label>
                                        <input required type="email" name="emailvendeur" id="materialRegisterFormEmail" class="form-control">

                                    </div>
                                    <br>

                                    <!-- Password -->
                                    <div class="md-form">
                                        <label for="materialRegisterFormPassword" style="float: left;">Password</label>
                                        <input required type="password" name="passwordvendeur" id="materialRegisterFormPassword" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock">

                                        <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                                            At least 8 characters and 1 digit
                                        </small>
                                    </div>
                                    <!-- Type account -->
                                    <div class="md-form">
                                        <p>Type de compte</p>
                                        <div class="row d-flex" style="justify-content:space-evenly">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" checked="true" name="type_user" id="exampleRadios2" value="1">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Vendeur
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="type_user" id="exampleRadios3" value="2">
                                                <label class="form-check-label" for="exampleRadios3">
                                                    Client
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Phone number -->
                                    <div class="md-form">
                                        <label for="materialRegisterFormPhone" style="float: left;">Phone number</label>
                                        <input required type="text" name="telephonevendeur" id="materialRegisterFormPhone" class="form-control" aria-describedby="materialRegisterFormPhoneHelpBlock">

                                        <small id="materialRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
                                            Optional - for two step authentication
                                        </small>
                                    </div>
                                    <div class="md-form">
                                        <span class="input-group-text " style="background-color: #17a2b8!important;color:white" id="inputGroupFileAddon01">Upload</span>

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="imagevendeur">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>

                                    <!-- Sign up button -->
                                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Sign in</button>

                                    <!-- Social register -->
                                    <p>or sign up with:</p>

                                    <a type="button" class="btn-floating btn-fb btn-sm">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a type="button" class="btn-floating btn-tw btn-sm">
                                        <i class="fab fa-twitter"></i>

                                    </a>
                                    <a type="button" class="btn-floating btn-li btn-sm">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                    <a type="button" class="btn-floating btn-git btn-sm">
                                        <i class="fab fa-github"></i>
                                    </a>

                                    <hr>

                                    <!-- Terms of service -->
                                    <p>By clicking
                                        <em>Sign up</em> you agree to our
                                        <a href="" target="_blank">terms of service</a>

                                </form>
                                <!-- Form -->

                            </div>
                        </div>
                        <!--/.Panel 8-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>