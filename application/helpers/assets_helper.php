<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('css_url')){
	function css_url($nom)
	{
		return base_url() . 'assets/css/' . $nom . '.css';
	}	
}

if(!function_exists('import_url')){
	function import_url($nom)
	{
		return base_url() . 'importations/' . $nom;
	}	
} 

if(!function_exists('js_url'))
{
	function js_url($nom)
	{
		return base_url() . 'assets/js/' . $nom . '.js';
	} 
}

// helper des images du site --> les repertoriées dans assets
if(!function_exists('img_url'))
{
	function img_url($nom)
	{
		return base_url() . 'assets/img/' . $nom ;
	} 	
}

if(!function_exists('img'))
{
	function img($nom, $alt = '', $class='', $id='')
	{
 		return '<img src="' . img_url($nom) . '" alt="' . $alt . '" class="' . $class . '" id="' . $id . '" />';
 	}	
}

// vidéo
if(!function_exists('video'))
{
	function video($nom, $autoload = '', $class='', $id='',$loop='')
	{
 		return '<video controls loop = "'.$loop.'" src="' . img_url($nom) . '" class="' . $class . '" id="' . $id . '" ></video>';
 	}	
}

if(!function_exists('video_url'))
{
	function video_url($nom)
	{
		return base_url() . 'assets/img/' . $nom ;
	} 	
}


// helper des images des articles du site --> les repertoriées dans uploads
if(!function_exists('img_url_upload'))
{
	function img_url_upload($nom)
	{
		return base_url() . 'uploads/' . $nom ;
	} 	
}

if(!function_exists('img_upload'))
{
	function img_upload($nom, $alt = '', $class='', $id='')
	{
 		return '<img src="' . img_url_upload($nom) . '" alt="' . $alt . '" class="' . $class . '" id="' . $id . '" />';
 	}	
}
